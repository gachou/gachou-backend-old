// jest.config.ts
import type { Config } from "@jest/types";

// Sync object
const config: Config.InitialOptions = {
	coverageThreshold: {
		global: {
			branches: 50,
			functions: 80,
			lines: 80,
			statements: 80,
		},
	},
	globalSetup: "./test-setup/globalSetup.ts",
	setupFilesAfterEnv: ["./test-setup/setupFilesAfterEnv.ts"],
	testEnvironment: "node",
	testTimeout: 20000,
};
export default config;
