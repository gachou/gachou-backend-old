# @gachou/gachou-backend

[![NPM version](https://img.shields.io/npm/v/@gachou/gachou-backend.svg)](https://npmjs.com/package/@gachou/gachou-backend)

> gachou-backend

This module implements the ReSTful backend of the gachou project.

It contains implementations for most of the interfaces contained in
the [gachou-api](https://gitlab.com/gachou/gachou-api) module.

That is, there is an implementation of the core-api as well as default
implementations for all plugins.

As the work proceeds, plugins may be extracted into their own
projects. The core-api will remain here. It's job is to tie together
the plugins and implement workflows that call the right plugins at the
right time. (i.e. Download the original file from the media-store,
create a thumbnail, upload the thumbnail to the store)

Plugins implement the actual work (like creating thumbnails or
extracting metadata).

The implementation of the tasks-api uses a simple queue at the moment.
In the future, it might be possible to change this into a distributed
system with a full-featured message-queue and worker nodes.

# Installation

```
npm install @gachou/gachou-backend
```

# License

`@gachou/gachou-backend` is published under the MIT-license.

See [LICENSE](LICENSE) for details.

# Contributing guidelines

See [CONTRIBUTING.md](CONTRIBUTING.md).
