import { exiftool } from "../src/utils/exiftool-utils";
import "../src/internal/test-utils/image-snapshot-presets";

afterAll(() => exiftool.end());
