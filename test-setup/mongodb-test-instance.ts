// tests/db-handler.js

import mongoose, { Connection, ConnectionOptions } from "mongoose";
import { MongoMemoryServer } from "mongodb-memory-server";

interface UseMongoDbTestInstanceResult {
	mongoDbUrl: () => string;
	clearDatabase: () => void;
}

export function useMongoDbTestInstance(): UseMongoDbTestInstanceResult {
	const mongoDb: MongoMemoryServer = new MongoMemoryServer();
	let connection: Connection;

	let currentUrl: string;

	beforeAll(async () => {
		await mongoDb.start();
		currentUrl = await mongoDb.getUri();
		const mongooseOpts: ConnectionOptions = {
			useNewUrlParser: true,
			useUnifiedTopology: true,
		};
		connection = await mongoose.createConnection(currentUrl, mongooseOpts);
	});

	async function clearDatabase(): Promise<void> {
		for (const collection of await connection.db.collections()) {
			await collection.deleteMany({});
		}
	}

	afterAll(async () => {
		await connection.close();
		await mongoDb.stop();
	});

	return { mongoDbUrl: () => currentUrl, clearDatabase };
}
