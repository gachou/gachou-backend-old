declare module "smartcrop-sharp" {
	export interface ISmartCropResult {
		topCrop: {
			x: number;
			y: number;
			height: number;
			width: number;
		};
	}

	export interface ICropSpec {
		height: number;
		width: number;
	}

	export function crop(image: string | Buffer, spec: ICropSpec): Promise<ISmartCropResult>;

	export function crop(
		image: string | Buffer,
		spec: ICropSpec,
		callback: (err, result: ISmartCropResult) => void
	): void;
}
