#!/bin/bash -x

# We want gachou to be able to write to the media directory.
# So let's ensure that the user in the docker-container is that
# user and has that group

# Check own user and group of the media-directory
GACHOU_GID="$( stat -c "%g" /gachou/media )"
GACHOU_UID="$( stat -c "%u" /gachou/media )"

GACHOU_USER="$(getent passwd "${GACHOU_UID}" | cut -d ":" -f 1 )"
GACHOU_GROUP="$(getent group "${GACHOU_GID}" | cut -d ":" -f 1 )"

# If there is no such group, create one
if [ -z "${GACHOU_GROUP}" ] ; then
    GACHOU_GROUP=gachou
    addgroup --system --gid="${GACHOU_GID}" gachou
fi

# If there is no such user, create one
if [ -z "${GACHOU_USER}" ] ; then
    GACHOU_USER=gachou
    adduser --uid="${GACHOU_UID}" \
        --home "${HOME}" \
        --ingroup="${GACHOU_GROUP}" \
        --shell /sbin/nologin \
        --disabled-password \
        --gecos '' \
        "${GACHOU_USER}"
fi

chown -R ${GACHOU_USER}.${GACHOU_GROUP} /gachou/cache

gosu ${GACHOU_USER} "$@"

