module.exports = {
	presets: [
		[
			"@babel/preset-env",
			{
				targets: {
					node: "current",
				},
			},
		],
		"@babel/preset-typescript",
	],
	env: {
		production: {
			// For node.js projects, we want to enable comments to have a better readable code in the end
			comments: true,
			ignore: ["src/**/*.test.ts", "src/internal/test-utils/**", "src/**/__mocks__/**"],
		},
	},
};
