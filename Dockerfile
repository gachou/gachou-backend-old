# ---------- compile typescript to javascript ---------------
FROM node:12 AS compile

ENV BUILD_AREA /build_area
COPY package.json yarn.lock $BUILD_AREA/
WORKDIR $BUILD_AREA
RUN yarn install --frozen-lockfile
COPY tsconfig.* babel.config.js $BUILD_AREA/
COPY src/ $BUILD_AREA/src/
RUN ls -l
RUN yarn build

# ---------- build docker image ---------------
FROM node:12

ENV APP_HOME /gachou/app

ADD https://github.com/tianon/gosu/releases/download/1.10/gosu-amd64 /usr/local/bin/gosu
RUN chmod a+x /usr/local/bin/gosu
# From: https://gist.github.com/alkrauss48/2dd9f9d84ed6ebff9240ccfa49a80662
# Set the home directory to our app user's home.
RUN mkdir -p $APP_HOME
WORKDIR $APP_HOME

COPY package.json yarn.lock $APP_HOME/
RUN yarn install --frozen-lockfile --production
COPY docker-setup /docker-setup
COPY --from=compile /build_area/dist $APP_HOME/dist/

VOLUME /gachou/media
VOLUME /gachou/cache
EXPOSE 8080
ENV GACHOU_CONFIG_MONGO_DB_URL="mongodb://mongodb/gachou"
ENV GACHOU_CONFIG_MONGO_DB_USERNAME=""
ENV GACHOU_CONFIG_MONGO_DB_PASSWORD=""
ENTRYPOINT ["/docker-setup/entrypoint.sh"]
CMD ["/usr/local/bin/node", "dist/server-cli.js", "--config", "/docker-setup/gachou-config.json"]
