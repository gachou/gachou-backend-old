import { ExifDateTime, exiftool } from "../utils/exiftool-utils";
import { loadFixture, temporaryDirectoryForTest } from "@gachou/testdata";
import { Mp4Normalizer } from "./mp4-normalizer";
import { ISSIMResult, ssim } from "../internal/test-utils/ssim";
import { parseDate } from "../metadata-handler/converter";
import { DateTime } from "luxon";

const tmpDir = temporaryDirectoryForTest(module);

describe("The mp4-normalizer", function () {
	beforeEach(async () => tmpDir.refresh());

	it("should copy data and metadata from mp4-videos and keep the file name", async function () {
		const tmpFile = tmpDir.resolve("video.mp4");
		const originalFile = tmpDir.resolve("video-original.mp4");
		await loadFixture("basic/1-video-streamable.mp4").andCopyTo(originalFile);
		await loadFixture("basic/1-video-streamable.mp4").andCopyTo(tmpFile);
		await exiftool.write(tmpFile, { HierarchicalSubject: "abc/bcd/cde" });

		// Check prerequisites
		const tags1 = await exiftool.read(tmpFile);
		expect(tags1.HierarchicalSubject).toEqual(["abc/bcd/cde"]);

		// Run the test
		const targetFile = await new Mp4Normalizer().normalize(tmpFile, "video/mp4", new Date());

		// Assertions
		expect(targetFile).toBe(tmpFile);
		const tags = await exiftool.read(targetFile);
		expect(tags.HierarchicalSubject).toEqual(["abc/bcd/cde"]);

		const issimResult: ISSIMResult = await ssim(originalFile, targetFile);
		expect(issimResult.all).toEqual(1);
	});

	it("should use the modification date as creation date for mts files", async function () {
		const tmpFile = tmpDir.resolve("lumix.mts");
		await loadFixture("basic/00000.MTS").andCopyTo(tmpFile);

		const modificationDate = new Date("1989-07-02T14:00:00+0200");
		const targetFile = await new Mp4Normalizer().normalize(tmpFile, "video/m2ts", modificationDate);
		expect(targetFile).toBe(tmpDir.resolve("lumix.mp4"));
		const tags = await exiftool.read(targetFile);
		expect(parseDate(tags.DateTimeOriginal)).toEqual(
			DateTime.fromISO("1989-07-02T14:00:00+0200", { setZone: true })
		);
		expect((tags.DateTimeOriginal as ExifDateTime).tzoffsetMinutes).toBe(
			-modificationDate.getTimezoneOffset()
		);
	});

	it("should ignore the modification date, if a create date is set", async function () {
		const tmpFile = tmpDir.resolve("video.mp4");
		await loadFixture("basic/1-video-streamable.mp4").andCopyTo(tmpFile);
		await exiftool.write(tmpFile, {
			CreateDate: "2013-07-02T17:00:00",
			GPSDateTime: "2013-07-02T14:00:00",
		});

		const targetFile = await new Mp4Normalizer().normalize(
			tmpFile,
			"video/mp4",
			new Date("2013-07-02T20:00:00+0200")
		);
		expect(targetFile).toBe(tmpFile);
		const tags = await exiftool.read(targetFile);
		expect((tags.CreateDate as ExifDateTime).tzoffsetMinutes).toBe(180);
		expect(parseDate(tags.CreateDate)?.toISO()).toBe("2013-07-02T17:00:00.000+03:00");
	});
});
