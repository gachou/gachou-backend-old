import fs from "fs-extra";
import { IMediaNormalizer } from "../api";
import { addCreationDateIfMissing } from "../utils/exiftool-utils";
import { replaceExtension } from "../utils/replace-extension";

const extensions = {
	"image/jpeg": "jpg",
	"image/png": "png",
};

/**
 * Converts video files to an mp4 format.
 */
export class ImageNormalizer implements IMediaNormalizer {
	public async bid(file: string, mimeType: string): Promise<number> {
		return extensions[mimeType] != null ? 0 : -1;
	}

	/**
	 * For now, we just make sure that the creation date is set.
	 *
	 * @param file
	 * @param mimeType
	 * @param modificationDate
	 */
	public async normalize(file: string, mimeType: string, modificationDate: Date): Promise<string> {
		await addCreationDateIfMissing(file, modificationDate);
		const targetFile = replaceExtension(file, extensions[mimeType]);
		if (targetFile !== file) {
			await fs.move(file, targetFile);
		}
		return targetFile;
	}
}
