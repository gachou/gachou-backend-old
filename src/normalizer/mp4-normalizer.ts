import fs from "fs-extra";
import { IMediaNormalizer } from "../api";
import { copyTags, addCreationDateIfMissing } from "../utils/exiftool-utils";
import { Ffmpeg, ffmpeg } from "../utils/ffmpeg";
import { ffprobe, IFfProbeStream } from "../utils/ffprobe";
import { replaceExtension } from "../utils/replace-extension";

const validCodecs = {
	audio: {
		aac: true,
	},
	subtitle: {
		mov_text: true,
	},
	video: {
		h264: true,
		hevc: true, // h265
	},
};

/**
 * Presets for audio, video and subtitle streams
 */
const argsFns: IArgFns = {
	audio: (i) => [
		`-c:${i}`,
		"aac", // Free encoder (libfdk_aac has an incompatible license)
		`-b:${i}`,
		"160k", // Constant bitrate because variable bitrate is experimental
	],
	subtitle: (i, probe) => {
		switch (probe.codec_name) {
			case "hdmv_pgs_subtitle":
			case "pgssub":
				// Subtitles are image based and cannot be converted to mov_text, so we ignore them
				// see https://stackoverflow.com/questions/36326790/cant-change-video-subtitles-codec-using-ffmpeg
				return [];
			default:
				return [`-c:${i}`, "mov_text"];
		}
	},
	video: (i) => [
		`-c:${i}`,
		"libx264",
		`-crf:${i}`,
		"17", // visually lossless
		`-preset:${i}`,
		"veryslow",
		`-tune:${i}`,
		"film", // high quality movies
	],
};

function streamHasValidCodec(streamProbe: IFfProbeStream) {
	return validCodecs[streamProbe.codec_type][streamProbe.codec_name] != null;
}

function addArgsForCopyingStream(ffmpegCmd: Ffmpeg, streamProbe: IFfProbeStream) {
	ffmpegCmd.addArgs(`-c:${streamProbe.index}`, "copy");
}

function addArgsForConvertingToValidCodec(ffmpegCmd: Ffmpeg, streamProbe: IFfProbeStream) {
	ffmpegCmd.addArgs(...argsFns[streamProbe.codec_type](streamProbe.index, streamProbe));
}

function addArgsForStreamableMp4(ffmpegCmd: Ffmpeg) {
	ffmpegCmd.addArgs("-movflags", "+faststart");
}

async function convertToMp4(file: string, intermediateFile: string) {
	const probe = await ffprobe(file);
	const ffmpegCmd = ffmpeg().from(file).addArgs("-threads", 1);
	probe.streams.forEach((streamProbe) => {
		if (streamHasValidCodec(streamProbe)) {
			addArgsForCopyingStream(ffmpegCmd, streamProbe);
		} else {
			addArgsForConvertingToValidCodec(ffmpegCmd, streamProbe);
		}
	});
	addArgsForStreamableMp4(ffmpegCmd);
	await ffmpegCmd.writeTo(intermediateFile);
}

/**
 * Converts video files to an mp4 format.
 */
export class Mp4Normalizer implements IMediaNormalizer {
	public async bid(file: string, mimeType: string): Promise<number> {
		return mimeType.match(/^video\//) ? 0 : -1;
	}

	public async normalize(file: string, mimeType: string, modificationDate: Date): Promise<string> {
		const targetFile = replaceExtension(file, "mp4");
		const intermediateFile = `${targetFile}.tmp.mp4`;
		await convertToMp4(file, intermediateFile);
		await copyTags(file, intermediateFile);
		await addCreationDateIfMissing(intermediateFile, modificationDate);
		await fs.remove(file);
		await fs.move(intermediateFile, targetFile);
		return targetFile;
	}
}

/**
 * Map of functions that compute arguments for different streams.
 * The key is the stream-type, value is a function returning a list
 * of arguments (string[]) for the ffprobe-output of the given stream
 * and the index-number of the stream.
 */
interface IArgFns {
	[type: string]: (index: number, probe: IFfProbeStream) => string[];
}
