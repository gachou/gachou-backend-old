import fs from "fs-extra";

import { ExifDateTime, exiftool } from "../utils/exiftool-utils";
import fixtureImages from "fixture-images";
import { compare } from "../internal/test-utils/compare-image";
import { temporaryDirectoryForTest } from "@gachou/testdata";
import { ImageNormalizer } from "./image-normalizer";
import { parseDate } from "../metadata-handler/converter";
import "trace";
const tmpDir = temporaryDirectoryForTest(module);

jest.setTimeout(10000);

describe("The image-normalizer", function () {
	beforeEach(async () => tmpDir.refresh());

	it("should accept image files", async function () {
		const normalizer = new ImageNormalizer();
		expect(await normalizer.bid(fixtureImages.path.still.png, "image/png")).toBe(0);
		expect(await normalizer.bid(fixtureImages.path.still.png, "image/jpeg")).toBe(0);
		expect(await normalizer.bid(fixtureImages.path.still.png, "image/svg")).toBe(-1);
	});

	it("should copy data and metadata from jpg-images and keep the file name", async function () {
		const imageFile = tmpDir.resolve("image.jpg");
		await fs.copy(fixtureImages.path.still.jpg, imageFile);
		const originalFile = fixtureImages.path.still.jpg;
		const diff = tmpDir.resolve("image-diff.png");
		await exiftool.write(imageFile, { HierarchicalSubject: ["abc/bcd/cde"] });

		const targetFile = await new ImageNormalizer().normalize(imageFile, "image/jpeg", new Date());
		expect(targetFile).toBe(imageFile);
		const tags = await exiftool.read(targetFile);
		// Existing tags should be preserved
		expect(tags.HierarchicalSubject).toEqual(["abc/bcd/cde"]);
		expect(await compare(originalFile, targetFile, diff, 0.1)).toBe(0);
	});

	it("should use the modification date as creation date for mts files", async function () {
		const imageFile = tmpDir.resolve("image.jpg");
		await fs.copy(fixtureImages.path.still.jpg, imageFile);
		const modificationDate = new Date("1989-07-02T14:00:00+0200");
		const targetFile = await new ImageNormalizer().normalize(
			imageFile,
			"image/jpeg",
			modificationDate
		);
		const tags = await exiftool.read(targetFile);
		const exifCreationDate = parseDate(tags.DateTimeOriginal);
		expect(exifCreationDate?.toISO()).toBe("1989-07-02T14:00:00.000+02:00");
		expect((tags.DateTimeOriginal as ExifDateTime)?.tzoffsetMinutes).toBe(
			-modificationDate.getTimezoneOffset()
		);
	});

	it("should ignore the modification date, if a create date is set", async function () {
		const imageFile = tmpDir.resolve("image.jpg");
		await fs.copy(fixtureImages.path.still.jpg, imageFile);
		await exiftool.write(imageFile, {
			DateTimeOriginal: "2013-07-02T17:00:00",
			GPSDateTime: "2013-07-02T14:00:00",
		});

		// Run with arbitary "modificationDate"
		const targetFile = await new ImageNormalizer().normalize(
			imageFile,
			"image/jpeg",
			new Date("2013-07-02T20:00:00+0200")
		);
		const tags = await exiftool.read(targetFile);
		expect(parseDate(tags.CreateDate)?.toISO()).toBe("2013-07-02T17:00:00.000+03:00");
		expect(parseDate(tags.DateTimeOriginal)?.toISO()).toBe("2013-07-02T17:00:00.000+03:00");
		expect((tags.CreateDate as ExifDateTime).tzoffsetMinutes).toBe(180);
	});
});
