import cuid from "cuid";
import fs from "fs-extra";
import path from "path";
import { Filename, IParsedScaleSpec, IThumbnailer, ScaleSpec } from "../api";
import { Auction } from "./auction";

/**
 * Choose the most appropriate of a list of thumbnailer for a media file
 */
export class ThumbnailerFacade extends Auction<IThumbnailer> {
	private readonly tmpDir: string;

	/**
	 * Create a new instance ... and the tmpdir, if needed
	 * @param tmpDir the tmpDir of the gachou-config
	 * @param delegates
	 */
	constructor(delegates: IThumbnailer[], tmpDir: string) {
		super(delegates);
		this.tmpDir = path.join(tmpDir, "thumbnailer");
		fs.mkdirsSync(this.tmpDir);
	}

	/**
	 * Create a new thumbnail
	 * @param file
	 * @param spec
	 */
	public async createThumbnail(file: Filename, spec: ScaleSpec): Promise<Filename> {
		const parsedSpec = parseSpec(spec);
		return this.run(file, async (bestThumbnailer) => {
			const thumbFilePrefix = path.join(this.tmpDir, cuid());
			return bestThumbnailer.createThumbnail(file, parsedSpec, thumbFilePrefix);
		});
	}

	protected noDelegateFound(file: Filename, mimeType: string): Error {
		return new Error("No thumbnailer found for file: " + file + ", mimetype: " + mimeType);
	}
}

/**
 * Returns an object containing the components of a scale specification
 * @param {string} spec
 * @return {{width: number, height: number}}
 */
function parseSpec(spec: ScaleSpec): IParsedScaleSpec {
	const match = spec.match(/^(\d+)x(\d+)(-crop)?$/);
	if (match) {
		return {
			width: Number(match[1]),
			height: Number(match[2]),
			crop: Boolean(match[3]),
		};
	}
	throw new Error("Could not parse scale specificiton: " + spec);
}
