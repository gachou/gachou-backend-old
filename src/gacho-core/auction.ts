import { Filename, IBidder } from "../api";
import { detectMimeType } from "../utils/exiftool-utils";

/**
 * Abstract base class for an auction in which multiple bidders vote
 * for handling a file the action to the most suitable of a list of delegates
 */
export abstract class Auction<T extends IBidder> {
	private delegates: T[];

	constructor(delegates: T[]) {
		this.delegates = delegates;
	}

	/**
	 * Determine the best bidder for a file, then delegate work to it.
	 * @param file the filename
	 * @param callback the callback performing the actual task. Receives the best bidder as parameter
	 */
	protected async run<R>(
		file: Filename,
		callback: (best: T, mimeType: string) => Promise<R>
	): Promise<R> {
		const mimeType = await detectMimeType(file);
		const bids = await Promise.all(this.delegates.map((delegate) => delegate.bid(file, mimeType)));
		let bestIndex = -1;
		let bestBid = -1;
		bids.forEach((bid, index) => {
			if (bid != null && bid > bestBid) {
				bestBid = bid;
				bestIndex = index;
			}
		});
		if (bestBid >= 0) {
			return callback(this.delegates[bestIndex], mimeType);
		}
		throw this.noDelegateFound(file, mimeType);
	}

	protected noDelegateFound(file: Filename, mimeType: string): Error {
		return new Error("No implementation found for file: " + file + ", mimetype: " + mimeType);
	}
}
