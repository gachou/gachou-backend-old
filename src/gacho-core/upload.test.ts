import { createTestServer, ITestServer } from "../internal/test-utils/test-server";
import * as fs from "fs-extra";
import { useMongoDbTestInstance } from "../../test-setup/mongodb-test-instance";
import { temporaryDirectoryForTest } from "@gachou/testdata";
import { IMetadataEntry, ISearchResult } from "../api";

const tmpDir = temporaryDirectoryForTest(module);
jest.setTimeout(120000);

describe("File upload", function () {
	let testServer: ITestServer;

	const { clearDatabase, mongoDbUrl } = useMongoDbTestInstance();

	beforeEach(async function () {
		await clearDatabase();
		await tmpDir.refresh();
		testServer = await createTestServer(
			tmpDir.resolve("server"),
			mongoDbUrl(),
			tmpDir.resolve("client")
		);
	});

	afterEach(() => testServer.shutdown());

	it("should store and scale the uploaded image", async function () {
		await testServer.uploadTestFixture({
			fileName: "video.mp4",
			fixtureName: "basic/1-video-streamable.mp4",
			modificationDate: "2018-06-25T16:00:00+0200",
		});

		const searchResult = await testServer.getJson<ISearchResult>("metadata");
		expect(searchResult.results.length).toEqual(1);

		const entry = searchResult.results[0];
		expect(entry.metadata.creationDate).toEqual("2018-06-25T16:00:00.000+02:00");
		expect(fs.existsSync(tmpDir.resolve("media", "2018", "06", entry.name)));

		// Test GET /metadata
		const entryReloaded = await testServer.getJson<IMetadataEntry>(`metadata/${entry.name}`);
		expect(entryReloaded).toEqual(entry);

		const searchAgainResult = await testServer.getJson<ISearchResult>("metadata");
		expect(searchAgainResult.results.length).toEqual(1);
	});

	it("should use the modification date passed as 'modificationDate'-parameter of an uploaded mts-file", async function () {
		await testServer.uploadTestFixture({
			fileName: "video.mts",
			fixtureName: "basic/00000.MTS",
			modificationDate: "2018-06-25T16:00:00+0200",
		});

		const searchResult = await testServer.getJson<ISearchResult>("metadata");
		expect(searchResult.results.length).toEqual(1);

		const entry = searchResult.results[0];
		expect(entry.metadata.creationDate).toEqual("2018-06-25T16:00:00.000+02:00");
		expect(entry.metadata.mimeType).toEqual("video/mp4");
		expect(fs.existsSync(tmpDir.resolve(`media/2018/06/${entry.name}`)));
	});

	it("should reject an upload that is missing a modificationDate", async function () {
		const response = await testServer.uploadTestFixture({
			fileName: "video.mp4",
			fixtureName: "basic/1-video-streamable.mp4",
		});
		expect(response.statusCode).toEqual(400);
		expect(response.body).toEqual({
			code: 400,
			message: "Invalid parameter value",
			parameters: [
				{
					message: "value is not a valid ISO-DateTime",
					paramName: "modificationDate",
					value: null,
				},
			],
		});
	});
});
