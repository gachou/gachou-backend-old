import {
	ICoreApi,
	IGachouConfig,
	IGachouStatus,
	IMediaFile,
	IMediaNormalizer,
	IMediaResponse,
	IMetadataEntry,
	IMetadataHandler,
	IMetadataIndex,
	IMetadataQuery,
	ISearchResult,
	IStoreApi,
	ITasksApi,
	IThumbnailer,
	ScaleSpec,
} from "../api";
import { createLogger } from "../utils/logger";
import { Tasks } from "./tasks";
import { HttpError } from "../utils/error";
import { Synchronizer } from "./synchronizer";
import { loggingProxy } from "./logging-proxy";

const logger = createLogger("gachou-backend:gachou-core");
const taskLogging = createLogger("gachou-backend:gachou-core-tasks");

export class GachouCore implements ICoreApi {
	private readonly tasks: ITasksApi;
	private readonly components: IGachouComponents;
	private readonly synchronizer: Synchronizer;

	constructor(components: IGachouComponents, config: IGachouConfig) {
		this.components = components;
		this.tasks = loggingProxy(
			new Tasks(components, config),
			["addFile", "normalize", "updateMetadata", "extractMetadata", "scaleImage"],
			taskLogging
		);
		this.synchronizer = new Synchronizer(components, this.tasks);
	}

	public async getOriginalMedium(name: string): Promise<IMediaResponse> {
		return this.components.mediaStore.get(name).catch(handleKnownErrors);
	}

	public async getThumbnail(name: string, spec: ScaleSpec): Promise<IMediaResponse> {
		try {
			return await this.components.mediaStore.getThumb(name, spec);
		} catch (error) {
			if (error.code === "ENOENT") {
				return this.tasks.scaleImage(name, spec).catch(handleKnownErrors);
			} else {
				throw error;
			}
		}
	}

	public find(query: IMetadataQuery, start: number, limit: number): Promise<ISearchResult> {
		return this.components.metadataIndex.find(query, start, limit);
	}

	public async getMetadata(name: string): Promise<IMetadataEntry> {
		const result = await this.components.metadataIndex.read(name);
		if (result == null) {
			throw new HttpError(404, `File not found`);
		}
		return result;
	}

	public addFile(file: string, modificationDate: Date): Promise<IMediaFile> {
		return this.tasks.addFile(file, modificationDate);
	}

	public async moveFileToTrash(name: string): Promise<void> {
		logger.info({ action: "moveFileToTrash", name });
		await this.components.metadataIndex.remove(name);
		try {
			await this.components.mediaStore.moveToTrash(name);
		} catch (error) {
			handleKnownErrors(error);
		}
		await this.components.mediaStore.removeThumbnails(name);
	}
	public async restoreFileFromTrash(name: string): Promise<void> {
		try {
			await this.components.mediaStore.restoreFromTrash(name);
		} catch (error) {
			handleKnownErrors(error);
		}
		await this.synchronizer.updateMetadataIndexAndThumbs(name);
	}

	public async updateMetadataIndex(): Promise<void> {
		return this.synchronizer.syncAllFiles();
	}

	public status(): IGachouStatus {
		return {
			tasks: this.tasks.status(),
			fullUpdateStatus: this.synchronizer.fullUpdateStatus,
		};
	}

	public async shutdown(): Promise<void> {
		try {
			await this.tasks.waitForIdle();
		} finally {
			await this.components.metadataIndex.shutdown();
		}
	}
}

function handleKnownErrors(error: unknown): never {
	if (error != null && (error as NodeJS.ErrnoException).code === "ENOENT") {
		throw new HttpError(404, "File not found");
	}
	throw error;
}

export interface IGachouComponents {
	mediaStore: IStoreApi;
	normalizer: IMediaNormalizer[];
	metadataHandler: IMetadataHandler;
	metadataIndex: IMetadataIndex;
	thumbnailers: IThumbnailer[];
}
