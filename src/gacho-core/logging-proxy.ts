import Pino from "pino";

function wrapWithLogger<Args extends unknown[], Returns>(
	target: unknown,
	propAsFn: (...args: Args) => Returns,
	logger: Pino.Logger,
	propertyKey: string | number | symbol
): (...args: Args) => Returns {
	return (...args: Args) => {
		function logAndReturn(resolvedResult: Returns): Returns {
			logger.debug({ type: "<<<<", method: propertyKey, args, resolvedResult });
			return resolvedResult;
		}

		function logAndThrow(error: Error): void {
			logger.debug({
				type: "!!!!",
				method: propertyKey,
				args,
				errorMessage: error.message,
				stack: error.stack,
			});
			throw error;
		}

		try {
			logger.debug({ type: ">>>>", method: propertyKey, args });
			const result = propAsFn.apply(target, args);
			if (result != null && typeof result.then === "function") {
				return result.then(logAndReturn, logAndThrow);
			}
			return logAndReturn(result);
		} catch (error) {
			logAndThrow(error);
		}
	};
}

// eslint-disable-next-line @typescript-eslint/ban-types
export function loggingProxy<T extends object>(
	delegate: T,
	loggedFunctions: (keyof T)[],
	logger: Pino.Logger
): T {
	return new Proxy<T>(delegate, {
		get(target, propertyKey) {
			const prop = Reflect.get(target, propertyKey);
			if (typeof prop === "function") {
				const propAsFn = prop as (...args: Parameters<typeof prop>) => ReturnType<typeof prop>;
				return wrapWithLogger(target, propAsFn, logger, propertyKey);
			}
			return prop;
		},
	});
}
