import { IGachouComponents } from "./gachou-core";
import { IFullUpdateStatus, ITasksApi } from "../api";
import { createLogger } from "../utils/logger";
import { HttpError } from "../utils/error";

const logger = createLogger("gachou-backend:synchronizer");

export class Synchronizer {
	private components: IGachouComponents;
	public fullUpdateStatus?: IFullUpdateStatus;
	private tasks: ITasksApi;

	constructor(components: IGachouComponents, tasks: ITasksApi) {
		this.components = components;
		this.tasks = tasks;
	}

	public async syncAllFiles(): Promise<void> {
		if (this.fullUpdateStatus?.lastStarted != null && this.fullUpdateStatus.lastFinished == null) {
			throw new HttpError(409, "Sync is already running");
		}
		this.fullUpdateStatus = {
			lastStarted: new Date(),
			totalFiles: -1,
			messages: [],
			done: 0,
		};
		try {
			this.fullUpdateStatus.totalFiles = await this.components.mediaStore.count();
			const syncId = await this.components.metadataIndex.syncStarted();

			for await (const name of this.components.mediaStore.iterate()) {
				try {
					if (this.fullUpdateStatus.abortRequested) {
						return;
					}
					await this.updateMetadataIndexAndThumbs(name);
					this.fullUpdateStatus.done++;
				} catch (error) {
					this.fullUpdateStatus.messages.push(error.message);
					logger.error(error);
				}
			}
			await this.components.metadataIndex.syncFinished(syncId);
		} catch (error) {
			this.fullUpdateStatus.fatalError = error.message;
		} finally {
			this.fullUpdateStatus.lastFinished = new Date();
		}
	}

	public async updateMetadataIndexAndThumbs(name: string): Promise<void> {
		const response = await this.components.mediaStore.get(name);
		const metadata = await this.tasks.extractMetadata(await response.file());
		await this.components.metadataIndex.add({ name, metadata });
		this.tasks.ensureAllThumbs(name);
	}
}
