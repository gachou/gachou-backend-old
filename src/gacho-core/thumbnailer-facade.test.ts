import fixtureImages from "fixture-images";
import { anyString, anything, deepEqual, instance, match, mock, verify, when } from "ts-mockito";
import { loadFixture, temporaryDirectoryForTest } from "@gachou/testdata";
import { Filename, IParsedScaleSpec, IThumbnailer } from "../api";
import { ThumbnailerFacade } from "./thumbnailer-facade";

const tmpDir = temporaryDirectoryForTest(module);

jest.setTimeout(10000);

// ATTENTION: This file uses mockito to mock the actual thumbnailer-classes.
// It only tests that the delagation works properly
describe("The thumbnailer-facade", function () {
	let mockMp4Thumbnailer: MockThumbnailer;
	let mockImageThumbnailer: MockThumbnailer;
	let thumbnailerFacade: ThumbnailerFacade;

	beforeEach(async () => {
		await tmpDir.refresh();
		mockMp4Thumbnailer = mock(MockThumbnailer);
		when(mockMp4Thumbnailer.bid(anyString(), "video/mp4")).thenResolve(0);
		when(mockMp4Thumbnailer.bid(anyString(), "image/jpeg")).thenResolve(-1);
		when(
			mockMp4Thumbnailer.createThumbnail(
				anyString(),
				deepEqual({
					height: 300,
					width: 300,
					crop: false,
				}),
				anyString()
			)
		).thenResolve("video-thumb.mp4");

		mockImageThumbnailer = mock(MockThumbnailer);
		when(mockImageThumbnailer.bid(anyString(), "video/mp4")).thenResolve(-1);
		when(mockImageThumbnailer.bid(anyString(), "image/jpeg")).thenResolve(0);
		when(
			mockImageThumbnailer.createThumbnail(
				anyString(),
				deepEqual({
					height: 300,
					width: 300,
					crop: false,
				}),
				anyString()
			)
		).thenResolve("image-thumb.jpg");

		thumbnailerFacade = new ThumbnailerFacade(
			[instance(mockMp4Thumbnailer), instance(mockImageThumbnailer)],
			tmpDir.resolve()
		);
	});

	describe("the 'createThumbnail'-method", function () {
		it("should delegate image-thumbnail requests to the image-thumbnailer", async function () {
			const thumbFile = await thumbnailerFacade.createThumbnail(
				fixtureImages.path.still.jpg,
				"300x300"
			);

			expect(thumbFile).toBe("image-thumb.jpg");
			verify(
				mockImageThumbnailer.createThumbnail(
					fixtureImages.path.still.jpg,
					deepEqual({ height: 300, width: 300, crop: false }),
					match(/thumbnailer\/c[\w\d]+/)
				)
			).called();
			verify(mockMp4Thumbnailer.createThumbnail(anything(), anything(), anything())).never();
		});

		it('should pass the "crop"-option to the thumbnailer', async function () {
			await thumbnailerFacade.createThumbnail(fixtureImages.path.still.jpg, "300x300-crop");
			verify(
				mockImageThumbnailer.createThumbnail(
					fixtureImages.path.still.jpg,
					deepEqual({ height: 300, width: 300, crop: true }),
					match(/thumbnailer\/c[\w\d]+/)
				)
			).called();
			verify(mockMp4Thumbnailer.createThumbnail(anything(), anything(), anything())).never();
		});

		it("should delegate mp4-thumbnail requests to the mp4-thumbnailer", async function () {
			const videoFile = tmpDir.resolve("video.mp4");
			await loadFixture("basic/1-video-streamable.mp4").andCopyTo(videoFile);
			const thumbFile = await thumbnailerFacade.createThumbnail(videoFile, "300x300");

			expect(thumbFile).toBe("video-thumb.mp4");
			verify(mockImageThumbnailer.createThumbnail(anything(), anything(), anything())).never();
			verify(
				mockMp4Thumbnailer.createThumbnail(
					videoFile,
					deepEqual({ height: 300, width: 300, crop: false }),
					match(/thumbnailer\/c[\w\d]+/)
				)
			).called();
		});

		it("should throw a proper error if no delegate could be found for the file type", async function () {
			try {
				await thumbnailerFacade.createThumbnail(__filename, "300x300");
				fail(new Error("this statement should never be reached"));
			} catch (e) {
				expect(e.message).toMatch(/^No thumbnailer found for file: /);
			}
			verify(mockImageThumbnailer.createThumbnail(anything(), anything(), anything())).never();
			verify(mockMp4Thumbnailer.createThumbnail(anything(), anything(), anything())).never();
		});

		it("throws an error the thumbnail-spec could not be parsed", async function () {
			try {
				const videoFile = await loadFixture("basic/1-video-streamable.mp4").andCopyTo(
					tmpDir.resolve("video.mp4")
				);

				await thumbnailerFacade.createThumbnail(videoFile, "300x300-some-thing-wrong");
				fail(new Error("this statement should never be reached"));
			} catch (e) {
				expect(e.message).toMatch("Could not parse scale specificiton: 300x300-some-thing-wrong");
			}
			verify(mockImageThumbnailer.createThumbnail(anything(), anything(), anything())).never();
			verify(mockMp4Thumbnailer.createThumbnail(anything(), anything(), anything())).never();
		});
	});
});

class MockThumbnailer implements IThumbnailer {
	public async bid(_file: string, _mimeType: string): Promise<number> {
		throw new Error("This method should have been mocked!");
	}

	public createThumbnail(
		_originalFile: Filename,
		_spec: IParsedScaleSpec,
		_thumbFilePrefix: string
	): Promise<Filename> {
		throw new Error("This method should have been mocked!");
	}
}
