import { loggingProxy } from "./logging-proxy";
import Pino from "pino";
import omit from "lodash/omit";

class Delegate {
	method(arg1: number, arg2: string): string {
		return arg1 + " " + arg2;
	}

	async asyncMethod(arg1: number, arg2: string): Promise<string> {
		return arg1 + " " + arg2;
	}

	methodThrowingError() {
		throw new Error("test-error");
	}

	async asyncMethodThrowingError() {
		throw new Error("test-error");
	}
}

test("Logs method that return a value", () => {
	const { collector, logger } = createTestLogger();
	const proxy = loggingProxy(new Delegate(), ["method"], logger);

	expect(proxy.method(1, "eins")).toEqual("1 eins");
	expect(collector).toEqual([
		{
			args: [1, "eins"],
			level: 20,
			method: "method",
			type: ">>>>",
		},
		{
			type: "<<<<",
			args: [1, "eins"],
			level: 20,
			method: "method",
			resolvedResult: "1 eins",
		},
	]);
});
test("Logs method that return a resolved promise", async () => {
	const { collector, logger } = createTestLogger();
	const proxy = loggingProxy(new Delegate(), ["asyncMethod"], logger);

	expect(await proxy.asyncMethod(1, "eins")).toEqual("1 eins");
	expect(collector).toEqual([
		{
			args: [1, "eins"],
			level: 20,
			method: "asyncMethod",
			type: ">>>>",
		},
		{
			type: "<<<<",
			args: [1, "eins"],
			level: 20,
			method: "asyncMethod",
			resolvedResult: "1 eins",
		},
	]);
});

test("Logs method that throws an error", async () => {
	const { collector, logger } = createTestLogger();
	const proxy = loggingProxy(new Delegate(), ["methodThrowingError"], logger);

	expect(() => proxy.methodThrowingError()).toThrow("test-error");
	expect(collector).toEqual([
		{
			args: [],
			level: 20,
			method: "methodThrowingError",
			type: ">>>>",
		},
		{
			args: [],
			errorMessage: "test-error",
			level: 20,
			method: "methodThrowingError",
			type: "!!!!",
			stack: "Error: test-error",
		},
	]);
});

test("Logs method that returns a rejected promise", async () => {
	const { collector, logger } = createTestLogger();
	const proxy = loggingProxy(new Delegate(), ["methodThrowingError"], logger);

	await expect(proxy.asyncMethodThrowingError()).rejects.toThrow("test-error");
	expect(collector).toEqual([
		{
			args: [],
			level: 20,
			method: "asyncMethodThrowingError",
			type: ">>>>",
		},
		{
			args: [],
			errorMessage: "test-error",
			level: 20,
			method: "asyncMethodThrowingError",
			type: "!!!!",
			stack: "Error: test-error",
		},
	]);
});

function createTestLogger(): { logger: Pino.Logger; collector: unknown[] } {
	const collector: unknown[] = [];
	const logger = Pino(
		{ level: "debug" },
		{
			write: (message) => {
				const reproducibleMessage = removeVariableParts(JSON.parse(message));
				collector.push(reproducibleMessage);
			},
		}
	);
	return { collector, logger };
}

function removeVariableParts(logObject: Record<string, unknown>) {
	const result = omit(logObject, "time", "pid", "hostname");
	if ("stack" in result) {
		const stack = result.stack as string | undefined;
		result.stack = stack?.split("\n")[0];
	}
	return result;
}
