import fixtures from "fixture-images";
import { anyOfClass, anyString, deepEqual, instance, mock, verify, when } from "ts-mockito";
import { IMediaNormalizer, NoNormalizerFoundError } from "../api";
import { NormalizerFacade } from "./normalizer-facade";

const testTmpDir = "tmp/normalizer-facade/";

describe("The normalizer-facade", () => {
	it("should throw an error if no normalizer can handle the file", async function () {
		const fixture = fixtures.path.still.png;
		const mock1: IMediaNormalizer = mock(MockNormalizer);
		when(mock1.bid(fixture, "image/png")).thenResolve(-1);
		const mock2: IMediaNormalizer = mock(MockNormalizer);
		when(mock2.bid(fixture, "image/png")).thenResolve(-2);

		const normalizer = new NormalizerFacade([instance(mock1), instance(mock2)], testTmpDir);
		await expect(normalizer.normalize(fixture, new Date(1000))).rejects.toThrow(
			NoNormalizerFoundError
		);

		verify(mock1.bid(fixture, "image/png")).called();
		verify(mock2.bid(fixture, "image/png")).called();
	});

	it("should use the normalizer with the highest bid", async function () {
		const fixture = fixtures.path.still.png;

		const mock1: IMediaNormalizer = mock(MockNormalizer);
		when(mock1.bid(fixture, "image/png")).thenResolve(1);
		when(mock1.normalize(anyString(), "image/png", anyOfClass(Date), testTmpDir)).thenResolve(
			"abc.png"
		);

		const mock2: IMediaNormalizer = mock(MockNormalizer);
		when(mock2.bid(fixture, "image/png")).thenResolve(3);
		when(mock2.normalize(anyString(), "image/png", anyOfClass(Date), testTmpDir)).thenResolve(
			"bcd.png"
		);

		const mock3: IMediaNormalizer = mock(MockNormalizer);
		when(mock3.bid(fixture, "image/png")).thenResolve(2);
		when(mock3.normalize(anyString(), "image/png", anyOfClass(Date), testTmpDir)).thenResolve(
			"cde.png"
		);

		const normalizer = new NormalizerFacade(
			[instance(mock1), instance(mock2), instance(mock3)],
			testTmpDir
		);
		expect(await normalizer.normalize(fixture, new Date(1000))).toBe("bcd.png");

		// Checking method calls on delegate normalizers
		verify(mock1.bid(fixture, "image/png")).called();
		verify(mock1.normalize(fixture, "image/png", deepEqual(new Date(1000)), testTmpDir)).never();
		verify(mock2.bid(fixture, "image/png")).called();
		verify(mock2.normalize(fixture, "image/png", deepEqual(new Date(1000)), testTmpDir)).once();
		verify(mock3.bid(fixture, "image/png")).called();
		verify(mock3.normalize(fixture, "image/png", deepEqual(new Date(1000)), testTmpDir)).never();
	});
});

/**
 * Used with mockito-mocks.
 */
abstract class MockNormalizer implements IMediaNormalizer {
	public bid(_file: string, _mimeType: string): Promise<number> {
		throw new Error('Called "bid" on actual object');
	}

	public normalize(
		_file: string,
		_mimeType: string,
		_modificationDate: Date,
		_tmpDir: string
	): Promise<string> {
		throw new Error('Called "normalize" on actual object');
	}
}
