/* tslint:disable:max-classes-per-file */
import fixtures from "fixture-images";
import { anyString, instance, mock, verify, when } from "ts-mockito";
import { Filename, IBidder } from "../api";
import { Auction } from "./auction";

describe("The delegating bidder", () => {
	it("should throw an noDelegateFound if no bidder can handle the file", async function () {
		const fixture = fixtures.path.still.png;
		const mock1: IMockup = mock(MockupImpl);
		when(mock1.bid(fixture, "image/png")).thenResolve(-1);
		const mock2: IMockup = mock(MockupImpl);
		when(mock2.bid(fixture, "image/png")).thenResolve(-2);

		const compositeMockup = new AuctionMockup([instance(mock1), instance(mock2)]);
		await expect(compositeMockup.someMethod(fixture)).rejects.toThrow(
			/No implementation found for file/
		);

		verify(mock1.bid(fixture, "image/png")).called();
		verify(mock2.bid(fixture, "image/png")).called();
	});

	it("should use the bidder with the highest bid", async function () {
		const fixture = fixtures.path.still.png;

		const mock1: IMockup = mock(MockupImpl);
		when(mock1.bid(fixture, "image/png")).thenResolve(1);
		when(mock1.someMethod(anyString())).thenResolve("abc.png");

		const mock2: IMockup = mock(MockupImpl);
		when(mock2.bid(fixture, "image/png")).thenResolve(3);
		when(mock2.someMethod(anyString())).thenResolve("bcd.png");

		const mock3: IMockup = mock(MockupImpl);
		when(mock3.bid(fixture, "image/png")).thenResolve(2);
		when(mock3.someMethod(anyString())).thenResolve("cde.png");

		const compositeMockup = new AuctionMockup([instance(mock1), instance(mock2), instance(mock3)]);
		expect(await compositeMockup.someMethod(fixture)).toBe("bcd.png");

		// Checking method calls on delegate normalizers
		verify(mock1.bid(fixture, "image/png")).called();
		verify(mock1.someMethod(fixture)).never();
		verify(mock2.bid(fixture, "image/png")).called();
		verify(mock2.someMethod(fixture)).once();
		verify(mock3.bid(fixture, "image/png")).called();
		verify(mock3.someMethod(fixture)).never();
	});
});

/**
 * Used with mockito-mocks.
 */
interface IMockup extends IBidder {
	someMethod(file: Filename): Promise<string>;
}

/**
 * Used with mockito-mocks.
 */
class MockupImpl implements IMockup {
	public bid(_file: Filename, _mimeType: string): Promise<number> {
		throw new Error('Called "bid" on actual object');
	}

	public someMethod(_file: Filename): Promise<string> {
		throw new Error('Called "someMethod" on actual object');
	}
}

class AuctionMockup extends Auction<IMockup> {
	public someMethod(file: Filename): Promise<string> {
		return this.run(file, (delegate) => delegate.someMethod(file));
	}
}
