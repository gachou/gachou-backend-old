import path from "path";
import { FsMediaStore } from "../media-store/fs-media-store";
import { ExiftoolMetadataHandler } from "../metadata-handler/exiftool-metadata-handler";
import { MongoDbMetadataIndex } from "../metadata-index/mongodb-metadata-index";
import { ImageNormalizer } from "../normalizer/image-normalizer";
import { Mp4Normalizer } from "../normalizer/mp4-normalizer";
import { FfmpegThumbnailer } from "../thumbnailer/ffmpeg-thumbnailer";
import { SharpThumbnailer } from "../thumbnailer/sharp-thumbnailer";
import { GachouCore } from "./gachou-core";
import { loggingProxy } from "./logging-proxy";
import { createLogger } from "../utils/logger";

const metadataLogger = createLogger("gachou-simple-setup:metadataIndex");

interface GachouSimpleSetupOptions {
	mediaDir: string;
	cacheDir: string;
	mongoDbUrl: string;
	mongoDbUsername?: string;
	mongoDbPassword?: string;
}

export async function setupGachou({
	mediaDir,
	cacheDir,
	mongoDbUrl,
	mongoDbUsername,
	mongoDbPassword,
}: GachouSimpleSetupOptions): Promise<GachouCore> {
	return new GachouCore(
		{
			mediaStore: await FsMediaStore.create(
				mediaDir,
				path.join(cacheDir, "thumbnails"),
				path.join(cacheDir, "trash")
			),
			metadataHandler: new ExiftoolMetadataHandler(),
			metadataIndex: loggingProxy(
				await MongoDbMetadataIndex.create(mongoDbUrl, {
					username: mongoDbUsername || undefined,
					password: mongoDbPassword || undefined,
				}),
				["add"],
				metadataLogger
			),
			normalizer: [new Mp4Normalizer(), new ImageNormalizer()],
			thumbnailers: [new SharpThumbnailer(), new FfmpegThumbnailer()],
		},
		{
			requiredThumbnails: ["800x200", "800x600"],
			tmpDir: cacheDir,
		}
	);
}
