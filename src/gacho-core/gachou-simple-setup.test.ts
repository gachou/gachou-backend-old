import path from "path";
import { loadFixture, temporaryDirectoryForTest } from "@gachou/testdata";
import { setupGachou } from "./gachou-simple-setup";

import { GachouCore } from "./gachou-core";
import { exiftool } from "../utils/exiftool-utils";
import { parseDate } from "../metadata-handler/converter";
import { waitUntil } from "../internal/test-utils/wait-until";
import { videoAsFilmstrip } from "../internal/test-utils/video-as-filmstrip";
import { HttpError } from "../utils/error";
import { useMongoDbTestInstance } from "../../test-setup/mongodb-test-instance";
import { DateTime } from "luxon";
import { IMediaFile } from "../api";

const testDataDir = temporaryDirectoryForTest(module);

beforeAll(() => testDataDir.refresh());

const { clearDatabase, mongoDbUrl } = useMongoDbTestInstance();

type TestSetup = { gachou: GachouCore; mediaDir: string };

const DATE = new Date("2020-10-28T05:17:33.386Z");
const DATE_TIME_WITH_FIXED_ZONE = DateTime.fromJSDate(DATE, { zone: "UTC+1" });

let setup: TestSetup;
beforeEach(async () => {
	await clearDatabase();
	setup = await setupGachouInUniqueDirectory();
}, 60000);

afterEach(async () => {
	await setup.gachou.shutdown();
	// await delay(1000);
}, 60000);

describe("the addFile-method", () => {
	it("addFile() returns the metadata of the uploaded file", async () => {
		const tmpFile = await loadFixture("basic/1-video-streamable.mp4").andCopyTo(
			testDataDir.resolve("abc")
		);
		const { name, metadata } = await setup.gachou.addFile(tmpFile, DATE);
		expect(name).toMatch(/2020-10-28__06-17-33-abc-\w+.mp4/);
		expect(metadata).toEqual({
			creationDate: DATE_TIME_WITH_FIXED_ZONE,
			extents: {
				height: 576,
				lengthMs: 2043,
				width: 1024,
			},
			tags: [],
			mimeType: "video/mp4",
		});
	});

	it("getOriginalMedium() throws an Not-Found Error", async () => {
		const errorResponse = setup.gachou.getOriginalMedium("2020-10-non-existing-file.mp4");
		await expect(errorResponse).rejects.toThrow(new HttpError(404, "File not found"));
	});

	it("getThumbnail() throws an Not-Found Error", async () => {
		const errorResponse = setup.gachou.getThumbnail("2020-10-non-existing-file.mp4", "300x300");
		await expect(errorResponse).rejects.toThrow(new HttpError(404, "File not found"));
	});

	it("getMetadata() throws an Not-Found Error", async () => {
		const errorResponse = setup.gachou.getMetadata("2020-10-non-existing-file.mp4");
		await expect(errorResponse).rejects.toThrow(new HttpError(404, "File not found"));
	});

	it("find() returns an empty result", async () => {
		const result = await setup.gachou.find({}, 0, 10);
		expect(result).toEqual({
			context: { limit: 10, start: 0, total: 0 },
			facets: [
				{ id: "year", label: "Year", values: [] },
				{ id: "month", label: "Month", values: [] },
				{ id: "mimeType", label: "Mime-Type", values: [] },
			],
			results: [],
		});
	});

	it("moveFileToTrash() returns an empty result", async () => {
		const errorResponse = setup.gachou.moveFileToTrash("2020-10-non-existing-file.mp4");
		await expect(errorResponse).rejects.toThrow(new HttpError(404, "File not found"));
	});

	it("restoreFileFromTrash() returns an empty result", async () => {
		const errorResponse = setup.gachou.restoreFileFromTrash("2020-10-non-existing-file.mp4");
		await expect(errorResponse).rejects.toThrow(new HttpError(404, "File not found"));
	});
});

async function waitUntilIdle(gachou: GachouCore) {
	await waitUntil(() => {
		const status = gachou.status();
		return status.tasks.running === 0 && status.tasks.waiting === 0;
	});
}

describe("after uploading a file", () => {
	let mediaFile: IMediaFile;

	beforeEach(async function (): Promise<void> {
		const tmpFile = await loadFixture("basic/1-video-streamable.mp4").andCopyTo(
			testDataDir.resolve("abc")
		);
		mediaFile = await setup.gachou.addFile(tmpFile, DATE);
		await waitUntilIdle(setup.gachou);
	}, 20000);

	it("getOriginalMedium() retrieves the normalized version", async () => {
		const original = await setup.gachou.getOriginalMedium(mediaFile.name);
		const tags = await exiftool.read(await original.file());
		expect(parseDate(tags.CreateDate)?.toISO()).toEqual("2020-10-28T06:17:33.386+01:00");
	});

	it("getThumbnail() retrieves a thumbnail without tags", async () => {
		const thumbnail = await setup.gachou.getThumbnail(mediaFile.name, "300x300");
		const tags = await exiftool.read(await thumbnail.file());
		expect(tags.CreateDate).toEqual("0000:00:00 00:00:00");
		expect(await videoAsFilmstrip(await thumbnail.file())).toMatchFilmstripSnapshot();
	});

	it("getMetadata() retrieves the image metadata", async () => {
		const metadata = await setup.gachou.getMetadata(mediaFile.name);
		expect(metadata).toEqual({
			metadata: {
				creationDate: DATE_TIME_WITH_FIXED_ZONE,
				extents: {
					height: 576,
					lengthMs: 2043,
					width: 1024,
				},
				rating: undefined,
				mimeType: "video/mp4",
				tags: [],
			},
			name: mediaFile.name,
		});
	});

	it("moveFileToTrash() and restoreFromTrash() delete and restore the image", async () => {
		await setup.gachou.moveFileToTrash(mediaFile.name);
		await expect(setup.gachou.getOriginalMedium(mediaFile.name)).rejects.toThrow(
			new HttpError(404, "File not found")
		);
		await setup.gachou.restoreFileFromTrash(mediaFile.name);
		await expect(setup.gachou.getOriginalMedium(mediaFile.name)).resolves.toBeTruthy();
	});
});

describe("when non-added files are present in the media-folder", () => {
	it("updateMetadataIndex() adds them", async () => {
		await loadFixture("basic/2016-08-02__11-00-53-p1050073.jpg").andCopyTo(
			setup.mediaDir,
			"2016",
			"08",
			"2016-08-02__11-00-53-p1050073.jpg"
		);

		expect((await setup.gachou.find({}, 0, 1)).results.length).toEqual(0);

		await setup.gachou.updateMetadataIndex();
		await waitUntilIdle(setup.gachou);

		expect((await setup.gachou.find({}, 0, 1)).results.length).toEqual(1);
	});
});

let gachouInstanceCounter = 0;
async function setupGachouInUniqueDirectory() {
	await clearDatabase();
	const currentCounterValue = gachouInstanceCounter++;
	const baseDir = testDataDir.resolve("gachou-" + currentCounterValue);
	const mediaDir = path.join(baseDir, "media");
	const cacheDir = path.join(baseDir, "cache");
	return {
		mediaDir,
		gachou: await setupGachou({
			mediaDir,
			cacheDir,
			mongoDbUrl: mongoDbUrl(),
		}),
	};
}
