function wrapToPreserveStack<Args extends unknown[], Returns>(
	target: unknown,
	propAsFn: (...args: Args) => Returns
): (...args: Args) => Returns {
	return (...args: Args) => {
		const result = propAsFn.apply(target, args);
		if (result != null && typeof result.catch === "function") {
			const stackBeforeCall = new Error().stack;
			return result.catch((error) => {
				error.stack += stackBeforeCall;
				throw error;
			});
		}
		return result;
	};
}

// eslint-disable-next-line @typescript-eslint/ban-types
export function preserveAsyncStack<T extends object>(delegate: T): T {
	return new Proxy<T>(delegate, {
		get(target, propertyKey) {
			const prop = Reflect.get(target, propertyKey);
			if (typeof prop === "function") {
				const propAsFn = prop as (...args: Parameters<typeof prop>) => ReturnType<typeof prop>;
				return wrapToPreserveStack(target, propAsFn);
			}
			return prop;
		},
	});
}
