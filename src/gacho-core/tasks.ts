import PQueue from "p-queue";
import {
	ITasksApi,
	Filename,
	IGachouConfig,
	IMediaResponse,
	IMetadata,
	ITaskStatus,
	ScaleSpec,
	IWritableMetadata,
} from "../api";
import { createLogger } from "../utils/logger";
import { IGachouComponents } from "./gachou-core";
import { NormalizerFacade } from "./normalizer-facade";
import { ThumbnailerFacade } from "./thumbnailer-facade";

const logger = createLogger("gachou-backend:tasks");

// TODO: refactor to use workers without p-queue but persistent task lists
export class Tasks implements ITasksApi {
	private queue: PQueue;
	private components: IGachouComponents;
	/**
	 * Queue for tasks that are composed of other tasks. In order to avoid deadlocks
	 */
	private compositeTaskQueue: PQueue;
	private config: IGachouConfig;
	private thumbnailer: ThumbnailerFacade;
	private normalizer: NormalizerFacade;

	constructor(components: IGachouComponents, config: IGachouConfig) {
		this.components = components;
		this.thumbnailer = new ThumbnailerFacade(this.components.thumbnailers, config.tmpDir);
		this.normalizer = new NormalizerFacade(this.components.normalizer, config.tmpDir);
		this.queue = new PQueue({ concurrency: 2 });
		this.queue.onIdle().then(() => logger.info("Queue: All work is done"));
		this.compositeTaskQueue = new PQueue({ concurrency: 2 });
		this.compositeTaskQueue
			.onIdle()
			.then(() => logger.info("CompositeTaskQueue: All work is done"));
		this.config = config;
	}

	public extractMetadata(file: Filename): Promise<IMetadata> {
		return this.queue.add(() => this.components.metadataHandler.extractMetadata(file));
	}

	public normalize(file: Filename, modificationDate: Date): Promise<Filename> {
		return this.queue.add(() => this.normalizer.normalize(file, modificationDate));
	}

	public scaleImage(name: string, spec: ScaleSpec): Promise<IMediaResponse> {
		return this.queue.add(async () => {
			const original = await this.components.mediaStore.get(name);
			const scaledFile = await this.thumbnailer.createThumbnail(await original.file(), spec);
			await this.components.mediaStore.putThumb(name, spec, scaledFile);
			return this.components.mediaStore.getThumb(name, spec);
		});
	}

	public updateMetadata(name: string, metadata: Partial<IWritableMetadata>): Promise<void> {
		return this.queue.add(async () => {
			await this.components.mediaStore.update(name, async (tmpFile) => {
				await this.components.metadataHandler.updateMetadata(tmpFile, metadata);
			});
			await this.components.metadataIndex.update(name, metadata);
		});
	}

	public addFile(
		file: Filename,
		modificationDate: Date
	): Promise<{ metadata: IMetadata; name: string }> {
		return this.compositeTaskQueue.add(async () => {
			const normalizedFile: string = await this.normalize(file, modificationDate);
			const metadata = await this.extractMetadata(normalizedFile);
			const name = await this.components.mediaStore.add(
				normalizedFile,
				metadata.creationDate.toJSDate()
			);
			// Scale all image sizes
			await Promise.all(
				this.config.requiredThumbnails.map((scaleSpec) => this.scaleImage(name, scaleSpec))
			);
			await this.components.metadataIndex.add({ name, metadata });
			return { metadata, name };
		});
	}

	/**
	 * Start background tasks for creating all needed thumbnails of a picture, but
	 * ignore the result. Log errors to the debug log
	 * @param {string} name
	 */
	public ensureAllThumbs(name: string): void {
		this.config.requiredThumbnails.forEach((spec) => {
			this.components.mediaStore
				.getThumb(name, spec)
				.catch((error) => {
					if (error.code === "ENOENT") {
						return this.scaleImage(name, spec);
					}
					throw error;
				})
				.catch((error) => logger.error(error));
		});
	}

	public status(): ITaskStatus {
		return {
			running: this.queue.pending + this.compositeTaskQueue.pending,
			waiting: this.queue.size + this.compositeTaskQueue.size,
		};
	}

	public async waitForIdle(): Promise<void> {
		this.queue.clear();
		this.compositeTaskQueue.clear();

		await Promise.all([this.queue.onIdle(), this.compositeTaskQueue.onIdle()]);
	}
}
