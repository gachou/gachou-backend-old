import { Filename, IMediaNormalizer, NoNormalizerFoundError } from "../api";
import * as fs from "fs-extra";
import { Auction } from "./auction";

export class NormalizerFacade extends Auction<IMediaNormalizer> {
	private tmpDir: string;

	constructor(delegates: IMediaNormalizer[], tmpDir: string) {
		super(delegates);
		this.tmpDir = tmpDir;
		fs.mkdirsSync(this.tmpDir);
	}

	/**
	 * Normalize the file using the normalizer with the highest bid
	 * @param {string} file
	 * @param {Date} modificationDate
	 * @return {Promise<string>}
	 */
	public async normalize(file: Filename, modificationDate: Date): Promise<string> {
		return this.run(file, (delegate, mimeType) =>
			delegate.normalize(file, mimeType, modificationDate, this.tmpDir)
		);
	}

	protected noDelegateFound(file: Filename, mimeType: string): Error {
		throw new NoNormalizerFoundError("No normalizer found for file ", mimeType);
	}
}
