import { exiftool } from "./exiftool-utils";
import { temporaryDirectoryForTest, loadFixture } from "@gachou/testdata";
import { ffmpeg } from "./ffmpeg";

const tmpDir = temporaryDirectoryForTest(module);

describe("The ffmpeg-tool", () => {
	beforeEach(() => tmpDir.refresh());

	it("should read video data from mp4-videos", async function () {
		const tmpFile = tmpDir.resolve("video.mp4");
		const targetFile = tmpDir.resolve("video2.jpg");
		await loadFixture("basic/1-video-streamable.mp4").andCopyTo(tmpFile);
		await ffmpeg()
			.from(tmpFile)
			.addArgs("-ss", 0.5)
			.addArgs("-vframes", 1)
			.addArgs("-s", "100x100")
			.addArgs("-f", "image2")
			.writeTo(targetFile);

		// verify
		const tags = await exiftool.read(targetFile);
		expect(tags.ImageSize).toBe("100x100");
	});

	it("should emit events for output lines on stdout and stderr", async function () {
		const tmpFile = tmpDir.resolve("video.mp4");
		await loadFixture("basic/1-video-streamable.mp4").andCopyTo(tmpFile);
		const stdout: string[] = [];
		const stderr: string[] = [];
		await ffmpeg()
			.from(tmpFile)
			.on("stdout", (line: string) => stdout.push(line))
			.on("stderr", (line: string) => stderr.push(line))
			.addArgs("-loglevel", "info")
			.addArgs("-f", "null")
			.writeTo(tmpDir.resolve("video.mkv"));
		expect(stderr.join("\n")).toMatch(/frame=\s*50/);
		expect(stdout).toEqual([""]);
	});
});
