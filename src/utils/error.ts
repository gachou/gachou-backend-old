export class HttpError extends Error {
	private readonly statusCode: number;

	constructor(statusCode: number, message: string) {
		super(message);
		this.statusCode = statusCode;
	}

	public toJSON(): Record<string, unknown> {
		return {
			code: this.statusCode,
			message: this.message,
		};
	}
}

export interface IErrorInParameter {
	paramName: string;
	message: string;
	value: unknown;
}

/**
 * An error that results in HTTP-400 because one or more
 * parameters have illegal values or are missing
 */
export class InvalidParametersError extends HttpError {
	private readonly params: IErrorInParameter[];
	constructor(message: string, params: IErrorInParameter[]) {
		super(400, message);
		this.params = params;
	}

	public toJSON(): Record<string, unknown> {
		return {
			...super.toJSON(),
			parameters: this.params,
		};
	}
}
