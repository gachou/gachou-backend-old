import { replaceExtension } from "./replace-extension";

test("adds the extension if no extension is present", () => {
	expect(replaceExtension("/path/to/test/abc", "mp4")).toEqual("/path/to/test/abc.mp4");
});

test("replaces the current extension if one is present", () => {
	expect(replaceExtension("/path/to/test/abc.jpg", "mp4")).toEqual("/path/to/test/abc.mp4");
});
