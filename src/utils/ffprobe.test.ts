import { loadFixture, temporaryDirectoryForTest } from "@gachou/testdata";
import { ffprobe } from "./ffprobe";

const tmpDir = temporaryDirectoryForTest(module);

describe("The ffprobe-tool", () => {
	beforeEach(async () => tmpDir.refresh());

	it("should read video data from mp4-videos", async function () {
		const tmpFile = tmpDir.resolve("video.mp4");
		await loadFixture("basic/1-video-streamable.mp4").andCopyTo(tmpFile);
		expect(await ffprobe(tmpFile)).toEqual({
			format: {
				duration: 2,
				format_name: "mov,mp4,m4a,3gp,3g2,mj2",
			},
			streams: [
				{
					codec_name: "h264",
					codec_type: "video",
					height: 576,
					index: 0,
					nb_frames: 50,
					width: 720,
				},
				{
					codec_name: "aac",
					codec_type: "audio",
					index: 1,
					nb_frames: 96,
				},
			],
		});
	});
});
