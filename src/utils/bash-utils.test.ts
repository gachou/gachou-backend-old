import { bashQuote } from "./bash-utils";

describe("The bash-utils", () => {
	describe("The bash-quote function", function () {
		it("should escape arguments as needed by bash", function () {
			expect(bashQuote(["ab", "a b", "a'b", "a' b"])).toBe("'ab' 'a b' 'a\\'b' 'a\\' b'");
		});
	});
});
