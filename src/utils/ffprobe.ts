import { execFile } from "child_process";
import Debug from "debug";

const debug = Debug("gachou-backend:ffprobe");
import { path as ffprobePath } from "ffprobe-static";
import {
	isArrayOfType,
	isNumber,
	isObjectOfType,
	isString,
	isNumberInString,
	validateThat,
	Validator,
	isOptional,
} from "./typecheck";
import { PathLike } from "fs";

export interface IFfProbeStream {
	codec_name: string;
	codec_type: string;
	index: number;
	nb_frames?: number;
	width?: number;
	height?: number;
}

const isFfProbeStream = isObjectOfType({
	codec_name: isString,
	codec_type: isString,
	nb_frames: isOptional(isNumberInString),
	index: isNumber,
	width: isOptional(isNumber),
	height: isOptional(isNumber),
});

export interface IFfProbeFormat {
	format_name: string;
	duration: number;
}

const isFfProbeFormat: Validator<IFfProbeFormat> = isObjectOfType({
	format_name: isString,
	duration: isNumberInString,
});

/**
 * Contains the result of ffprobe needed by gachou
 */
export interface IFfProbeResult {
	format: IFfProbeFormat;
	streams: IFfProbeStream[];
}

const isFfProbeResult: Validator<IFfProbeResult> = isObjectOfType({
	format: isFfProbeFormat,
	streams: isArrayOfType(isFfProbeStream),
});

export async function ffprobe(file: PathLike): Promise<IFfProbeResult> {
	return new Promise<IFfProbeResult>((resolve, reject) => {
		execFile(
			ffprobePath,
			["-print_format", "json", "-show_format", "-show_streams", String(file)],
			(err, stdout, stderr) => {
				if (err) {
					return reject(err);
				}
				if (stderr) {
					debug(stderr);
				}
				const parsedOutput = JSON.parse(stdout);
				try {
					resolve(validateThat(parsedOutput, isFfProbeResult));
				} catch (error) {
					reject(error);
				}
			}
		);
	});
}
