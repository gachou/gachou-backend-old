import { dim } from "./dimensions";

describe("The dimension-utils:", () => {
	it("should be comparable with deep-equal", function () {
		expect(dim(200, 200)).toEqual({ width: 200, height: 200 });
	});

	it("should compute the scaled dimensions to fit into a box", function () {
		expect(dim(400, 100).fitToBox({ width: 200, height: 200 })).toEqual({ width: 200, height: 50 });
		expect(dim(100, 400).fitToBox({ width: 200, height: 200 })).toEqual({ width: 50, height: 200 });
		expect(dim(100, 100).fitToBox({ width: 200, height: 200 })).toEqual({
			width: 200,
			height: 200,
		});
	});

	it("should compute the next larger size divisible by to", function () {
		expect(dim(101, 101).upToEvenNumbers()).toEqual({ width: 102, height: 102 });
		expect(dim(97, 400).upToEvenNumbers()).toEqual({ width: 98, height: 400 });
		expect(dim(100, 303).upToEvenNumbers()).toEqual({ width: 100, height: 304 });
		expect(dim(100, 300).upToEvenNumbers()).toEqual({ width: 100, height: 300 });
	});

	it("should format the output", function () {
		expect(dim(200, 100).format("%w+%h")).toBe("200+100");
		expect(dim(200, 100).format("%w:%h")).toBe("200:100");
	});

	it("should always round values", function () {
		expect(dim(200.1, 100.6)).toEqual({ width: 200, height: 101 });
	});
});
