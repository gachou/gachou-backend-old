import Pino, { DestinationStream } from "pino";
import fs from "fs-extra";
import { Writable } from "stream";

const defaultLoglevel = "info";

const channelLevels = {
	"batch-cluster": "error", // Avoid log pollution due to tags not being written
	"gachou-backend:gachou-core-tasks": "debug",
	"gachou-simple-setup:metadataIndex": "debug",
};

function createJestOutput(): DestinationStream {
	return new Writable({
		write(chunk, encoding, callback) {
			fs.appendFile("gachou-test.log", chunk, callback);
		},
	});
}

function createDefaultOutput(): DestinationStream {
	return process.stdout;
}

function createDestinationStream(): DestinationStream {
	if (process.env.JEST_WORKER_ID !== undefined) {
		return createJestOutput();
	}
	return createDefaultOutput();
}

const destinationStream = createDestinationStream();

export function createLogger(name: string): Pino.Logger {
	return Pino({ name, level: channelLevels[name] || defaultLoglevel }, destinationStream);
}
