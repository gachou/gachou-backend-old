import {
	isArrayOfType,
	isBoolean,
	isNumber,
	isNumberInString,
	isObjectOfType,
	isOptional,
	isRecord,
	isString,
	transformed,
	Validator,
	withDefaultValue,
} from "./typecheck";

function expectValidator<T>(validator: Validator<T>) {
	const assertions = {
		toPassThrough,
		toTransform,
		toReject,
		notToAcceptNullOrUndefined,
	};

	function toPassThrough(input: unknown) {
		it("returns the input value if it is " + JSON.stringify(input), () => {
			expect(validator(input)).toEqual(input);
		});
		return assertions;
	}

	function toTransform(input: unknown, expectedOutput: T) {
		it(`returns ${JSON.stringify(expectedOutput)} for input ${JSON.stringify(input)}`, () => {
			expect(validator(input)).toEqual(expectedOutput);
		});
		return assertions;
	}

	function toReject(input: unknown, errorMatch?: RegExp | string) {
		it(`throws "${errorMatch} for input ${JSON.stringify(input)}`, () => {
			expect(() => validator(input)).toThrow(errorMatch);
		});
		return assertions;
	}

	function notToAcceptNullOrUndefined() {
		it(`throws for null`, () => {
			expect(() => validator(null)).toThrow();
		});
		it(`throws for undefined`, () => {
			expect(() => validator(undefined)).toThrow();
		});
		return assertions;
	}
	return assertions;
}

describe("isString", () => {
	expectValidator(isString)
		.toPassThrough("abc")
		.toReject(1, '1 is not a string at ""')
		.notToAcceptNullOrUndefined();
});

describe("isNumber", () => {
	expectValidator(isNumber)
		.toPassThrough(1)
		.toReject("abc", '"abc" is not a number at ""')
		.notToAcceptNullOrUndefined();
});

describe("isBoolean", () => {
	expectValidator(isBoolean)
		.toPassThrough(true)
		.toPassThrough(false)
		.toReject("abc", '"abc" is not a boolean at ""')
		.notToAcceptNullOrUndefined();
});
describe("isObjectOfType", () => {
	expectValidator(isObjectOfType({ name: isString, age: isNumber }))
		.toPassThrough({ name: "Tom", age: 20 })
		.toReject({ name: "Tom", age: "20" }, '"20" is not a number at ".age"')
		.toReject("str", '"str" is not an object at ""')
		.notToAcceptNullOrUndefined();
});

describe("isArrayOfType", () => {
	expectValidator(isArrayOfType(isString))
		.toPassThrough(["a", "b", "c"])
		.toReject([1, 2, 3], '1 is not a string at "[0]"')
		.toReject(["a", 2, 3], '2 is not a string at "[1]"')
		.notToAcceptNullOrUndefined();
});

describe("optional", () => {
	expectValidator(isOptional(isString))
		.toPassThrough("abc")
		.toPassThrough(undefined)
		.toTransform(null, undefined)
		.toReject(1, '1 is not a string at ""');
});

describe("isNumberInString", () => {
	expectValidator(isNumberInString)
		.toTransform("2", 2)
		.toReject("abc", '"abc" is not a string containing a valid number at ""')
		.notToAcceptNullOrUndefined();
});

describe("transformed", () => {
	expectValidator(transformed(parseInt, isNumber)).toTransform("2", 2);
});

describe("withDefaultValue", () => {
	expectValidator(withDefaultValue([], isArrayOfType(isString)))
		.toTransform(undefined, [])
		.toTransform(null, [])
		.toPassThrough(["abc", "cde"])
		.toReject("abc");
});

describe("isRecord", () => {
	expectValidator(isRecord(isString))
		.toPassThrough({ a: "b", c: "d" })
		.toPassThrough({ x: "weg", y: "yeah" })
		.toReject({ a: 2 });
});

describe("combination", () => {
	expectValidator(
		isObjectOfType({
			name: isString,
			colors: isArrayOfType(isString),
			round: isOptional(isBoolean),
		})
	)
		.toPassThrough({ name: "apple", colors: ["red", "green", "yellow"] })
		.toPassThrough({ name: "apple", colors: ["red", "green", "yellow"], round: true })
		.toReject({ name: "apple", colors: ["red", "green", 0] }, '0 is not a string at ".colors[2]"')
		.toReject(["a", 2, 3], '["a",2,3] is not an object at ""')
		.notToAcceptNullOrUndefined();
});
