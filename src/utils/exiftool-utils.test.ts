import fs from "fs-extra";

import {
	addCreationDateIfMissing,
	copyTags,
	detectMimeType,
	ExifDateTime,
	exiftool,
	writableTags,
} from "./exiftool-utils";

import images from "fixture-images";
import { loadFixture, temporaryDirectoryForTest } from "@gachou/testdata";

const tmpDir = temporaryDirectoryForTest(module);

describe("The exiftool-utils:", () => {
	beforeEach(async () => tmpDir.refresh());

	describe("The 'writableTags'-property", function () {
		it("should contain a list of writable tags", async function () {
			expect(writableTags.MajorBrand).not.toEqual(true);
			expect(writableTags.CreateDate).toEqual(true);
		});
	});

	describe("The 'copyTags'-function", function () {
		it("should copy tags from one file to another", async function () {
			const sourceImage = tmpDir.resolve("sourceImage.jpg");
			const destImage = tmpDir.resolve("destImage.jpg");
			await fs.copy(images.path.still.jpg, sourceImage);
			await fs.copy(images.path.still.jpg, destImage);
			await exiftool.write(sourceImage, {
				Keywords: "abc",
				DateTimeOriginal: "2018-05-17T14:53:00+0200",
			});
			await copyTags(sourceImage, destImage);
			const tags = await exiftool.read(destImage);
			expect(tags.Keywords).toBe("abc");
			expect((tags.DateTimeOriginal as ExifDateTime)?.toISOString()).toBe(
				"2018-05-17T14:53:00.000"
			);
		});

		it("should not fail, if no tags have been copied", async function () {
			const destImage = tmpDir.resolve("destImage.jpg");
			const sourceFile = tmpDir.resolve("lumix.mts");
			await loadFixture("basic/00000.MTS").andCopyTo(sourceFile);
			await fs.copy(images.path.still.jpg, destImage);
			await copyTags(sourceFile, destImage);
			await copyTags(sourceFile, destImage);
		});
	});

	describe("The 'ensureCreationDate'-function", function () {
		it("should set a creation date for image that don't have one", async function () {
			const image = tmpDir.resolve("image.jpg");
			await fs.copy(images.path.still.jpg, image);
			await addCreationDateIfMissing(image, new Date("2018-05-17T14:53:00+0200"));
			const tags = await exiftool.read(image);
			expect((tags.DateTimeOriginal as ExifDateTime)?.toISOString()).toBe(
				"2018-05-17T14:53:00.000+02:00"
			);
			expect((tags.CreateDate as ExifDateTime)?.toISOString()).toBe(
				"2018-05-17T14:53:00.000+02:00"
			);
		});

		it("regression test for IMG_20160401_202342.jpg", async function () {
			const image = await loadFixture("basic/IMG_20160401_202342.jpg").andCopyTo(
				tmpDir.resolve("image.jpg")
			);
			await addCreationDateIfMissing(image, new Date("2018-05-17T14:53:00+0200"));
			const tags = await exiftool.read(image);
			expect((tags.DateTimeOriginal as ExifDateTime)?.toISOString()).toBe(
				"2016-04-01T20:23:43.000+02:00"
			);
			expect((tags.CreateDate as ExifDateTime)?.toISOString()).toBe(
				"2016-04-01T20:23:43.000+02:00"
			);
		});
	});

	describe("The detectMimeType", () => {
		beforeEach(() => tmpDir.refresh());

		it("should detect mts files as m2ts (not as octet-stream) ", async function () {
			const tmpFile = tmpDir.resolve("video.mts");
			await loadFixture("basic/00000.MTS").andCopyTo(tmpFile);
			expect(await detectMimeType(tmpFile)).toEqual("video/m2ts");
		});
	});
});
