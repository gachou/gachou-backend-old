export type NodeCallback<T> = (err: Error | undefined | null, result: T) => unknown;
export type NodeVoidCallback = (err: Error | undefined | null) => unknown;

/**
 * Returns a promise for a function with a node-style callback
 *
 * @param {(callback: INodeCallback<T>) => any} fn
 * @return {Promise<T>}
 */
export function valuePromise<T>(fn: (callback: NodeCallback<T>) => unknown): Promise<T> {
	return new Promise<T>((resolve, reject) => {
		fn((err, result: T) => {
			if (err != null) {
				reject(err);
			}
			resolve(result);
		});
	});
}

/**
 * Returns a promise for a function with a node-style-callaback, without result value
 * @param {(callback: Error)} fn
 */
export function voidPromise(fn: (callback: NodeVoidCallback) => unknown): Promise<void> {
	return new Promise((resolve, reject) => {
		fn((err) => {
			if (err != null) {
				reject(err);
			}
			resolve();
		});
	});
}
