interface IDimensions {
	readonly height: number;
	readonly width: number;
}

export class Dimensions implements IDimensions {
	public readonly height: number;
	public readonly width: number;

	constructor(width: number, height: number) {
		this.height = Math.round(height);
		this.width = Math.round(width);
	}

	public fitToBox(box: IDimensions): Dimensions {
		const widthRatio = box.width / this.width;
		const heightRatio = box.height / this.height;
		const maxRatio = Math.min(widthRatio, heightRatio);
		return new Dimensions(this.width * maxRatio, this.height * maxRatio);
	}

	/**
	 * Round up to the next even number. This is required by ffmpeg for mp4-encoding
	 */
	public upToEvenNumbers(): Dimensions {
		return new Dimensions(this.width + (this.width % 2), this.height + (this.height % 2));
	}

	public format(fmtString: string): string {
		return fmtString.replace(/%w/, String(this.width)).replace(/%h/, String(this.height));
	}
}

export function dim(width?: number, height?: number): Dimensions {
	if (height == null) {
		throw new Error("height must be a number");
	}
	if (width == null) {
		throw new Error("width must be a number");
	}

	return new Dimensions(width, height);
}
