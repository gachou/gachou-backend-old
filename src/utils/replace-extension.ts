import path from "path";

export function replaceExtension(filePath: string, newExtension: string): string {
	const basename = path.basename(filePath);
	const dirname = path.dirname(filePath);
	const extensionStartsAt = basename.lastIndexOf(".");
	if (extensionStartsAt < 0) {
		return path.join(dirname, basename + "." + newExtension);
	} else {
		return path.join(dirname, basename.substr(0, extensionStartsAt) + "." + newExtension);
	}

	return filePath.replace(/\.[^.]+$/g, "") + ".mp4";
}
