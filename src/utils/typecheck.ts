import { DateTime } from "luxon";

export type Validator<T> = (value: unknown, context?: string) => T;

class ValidationError extends Error {
	constructor(value: unknown, expectedType: string, context: string) {
		super(`${JSON.stringify(value)} is not ${expectedType} at "${context}"`);
	}
}

export const isNumber: Validator<number> = (value: unknown, context = ""): number => {
	if (typeof value === "number") {
		return value;
	}
	throw new ValidationError(value, "a number", context);
};

export const isNumberInString: Validator<number> = (value: unknown, context = ""): number => {
	if (typeof value === "number") {
		return value;
	}
	if (typeof value === "string") {
		const number = parseInt(value);
		if (isNaN(number)) {
			throw new ValidationError(value, "a string containing a valid number", context);
		}
		return number;
	}
	throw new ValidationError(value, "a string or a number", context);
};

export const isString: Validator<string> = (value: unknown, context = ""): string => {
	if (typeof value === "string") {
		return value;
	}
	throw new ValidationError(value, "a string", context);
};

export const isBoolean: Validator<boolean> = (value: unknown, context = ""): boolean => {
	if (typeof value === "boolean") {
		return value;
	}
	throw new ValidationError(value, "a boolean", context);
};

type ObjectValidationSpec<T> = {
	[P in keyof T]: Validator<T[P]>;
};

const asAnyObject: Validator<Record<string, unknown>> = (value, context = "") => {
	if (
		typeof value === "object" &&
		value != null &&
		value.constructor !== Date &&
		!Array.isArray(value)
	) {
		return value as Record<string, unknown>;
	}
	throw new ValidationError(value, "an object", context);
};

function withValidProperties<T>(
	validator: ObjectValidationSpec<T>,
	record: Readonly<Record<string, unknown>>,
	context: string
) {
	const result: Partial<T> = {};
	Object.keys(validator).forEach((key) => {
		result[key] = validator[key](record[key], `${context}.${key}`);
	});
	return result as T;
}

export function isObjectOfType<T>(validator: ObjectValidationSpec<T>): Validator<T> {
	return (value: unknown, context = "") => {
		const record = asAnyObject(value, context);
		return withValidProperties(validator, record, context);
	};
}

export function isRecord<K extends string, V>(validator: Validator<V>): Validator<Record<K, V>> {
	return (value: unknown, context = "") => {
		const record = asAnyObject(value, context);
		const result: Record<string, V> = {};
		Object.keys(record).forEach((key) => {
			result[key] = validator(record[key], context + "." + key);
		});
		return result;
	};
}

export function isArrayOfType<ItemType>(itemValidator: Validator<ItemType>): Validator<ItemType[]> {
	return (array: unknown, context = ""): ItemType[] => {
		if (Array.isArray(array)) {
			return array.map((item, index) => itemValidator(item, `${context}[${index}]`));
		}
		throw new Error(`${array} is not an array at "${context}"`);
	};
}

export function isOptional<T>(validator: Validator<T>): Validator<T | undefined> {
	return (value: unknown, context = ""): T | undefined => {
		if (value == null) {
			return undefined;
		}
		return validator(value, context);
	};
}

export function transformed<T>(
	transformer: (value: unknown) => T,
	validator: Validator<T>
): Validator<T> {
	return (value) => {
		try {
			return validator(transformer(value));
		} catch (error) {
			if (error instanceof ValidationError) {
				throw new Error("Validation of " + JSON.stringify(value) + " failed: " + error.message);
			} else {
				throw error;
			}
		}
	};
}

export const isDateTime: Validator<DateTime> = (value, context = "") => {
	if (DateTime.isDateTime(value)) {
		return value;
	}
	if (typeof value === "string") {
		return DateTime.fromISO(value);
	}
	throw new ValidationError(value, "a string or a DateTime", context);
};

export function withDefaultValue<T>(defaultValue: T, validator: Validator<T>): Validator<T> {
	return (value, context = "") => {
		if (value == null) {
			return defaultValue;
		}
		return validator(value, context);
	};
}

export function validateThat<T>(value: unknown, validator: Validator<T>): T {
	try {
		return validator(value);
	} catch (error) {
		if (error instanceof ValidationError) {
			throw new Error("Validation of " + JSON.stringify(value) + " failed: " + error.message);
		} else {
			throw error;
		}
	}
}
