import { spawn } from "child_process";
import Debug from "debug";
import { EventEmitter } from "events";
import ffmpegPath from "ffmpeg-static";
import split from "split";
import { bashQuote } from "./bash-utils";
import { PathLike } from "fs";

const debug = Debug("gachou-backend:ffmpeg");
const debugStderr = Debug("gachou-backend:ffmpeg:stderr");

export function ffmpeg(): Ffmpeg {
	return new Ffmpeg();
}

type OptionalString = string | null;

export class Ffmpeg extends EventEmitter {
	private args: string[];
	// Number of last lines from stderr to show on errors
	private stderrLimit = 10;

	constructor() {
		super();
		this.args = ["-y"];
	}

	/**
	 * Adds an input file
	 * @param {string} inputFile
	 * @return {this}
	 */
	public from(inputFile: PathLike): this {
		this.args.push("-i", String(inputFile));
		return this;
	}

	/**
	 * The number of stderr lines to show when an exceptions occurs
	 * @param stderrLimit
	 */
	public stderrLines(stderrLimit: number): this {
		this.stderrLimit = stderrLimit;
		return this;
	}

	public addArgs(...args: unknown[]): this {
		this.args.push(...args.map(String));
		return this;
	}

	/**
	 * Add some video filters
	 */
	public vf(...filters: OptionalString[]): this {
		// Add all non-null/non-undefined filters
		this.args.push("-vf", filters.filter((arg) => arg != null).join(","));
		return this;
	}

	/**
	 * For debugging and testing: Return the whole command line of ffmpeg
	 * @param targetFile
	 */
	public commandFor(targetFile: string): string {
		return bashQuote([ffmpegPath, ...this.args, targetFile]);
	}

	/**
	 * Run ffmpeg such that the stream is written to the targetFile
	 * @param targetFile
	 */
	public async writeTo(targetFile: string): Promise<string> {
		debug("Running ffmpeg with the following command line: ", this.commandFor(targetFile));
		const child = spawn(ffmpegPath, [...this.args, targetFile], {});
		const lastStderr: string[] = [];

		child.stderr
			.setEncoding("utf8")
			.pipe(split())
			.on("data", (line) => {
				this.emit("stderr", line);
				debugStderr(line);
				if (lastStderr.length >= this.stderrLimit) {
					lastStderr.shift();
				}
				lastStderr.push(line);
			});

		child.stdout
			.setEncoding("utf8")
			.pipe(split())
			.on("data", (line: string) => {
				this.emit("stdout", line);
				debugStderr(line);
			});

		await new Promise((resolve, reject) => {
			child.on("exit", (code) => {
				if (code === 0) {
					resolve(targetFile);
				} else {
					reject(
						new Error(
							`Unexpected exit code ${code} from\n${this.commandFor(
								targetFile
							)}\n\n${lastStderr.join("\n")}`
						)
					);
				}
			});
		});
		return targetFile;
	}
}
