/* eslint-disable no-restricted-imports */
import { execFileSync } from "child_process";
import { DefaultExifToolPath, ExifDateTime, ExifTool, Tags, WriteTags } from "exiftool-vendored";
import { setLogger } from "batch-cluster";
import fs from "fs-extra";
import { createLogger } from "./logger";
import { preserveAsyncStack } from "../gacho-core/preserve-async-stack";
import { replaceExtension } from "./replace-extension";
import cuid from "cuid";

export { ExifDateTime, Tags, WriteTags } from "exiftool-vendored";

setLogger(createLogger("batch-cluster"));

const exiftoolDelegate = preserveAsyncStack(
	new ExifTool({
		spawnTimeoutMillis: 20000,
		taskTimeoutMillis: 20000,
	})
);

export const exiftool = {
	async read(file: string, args?: string[]): Promise<Tags> {
		return await exiftoolDelegate.read(file, args);
	},
	async write(file: string, tags: WriteTags, args?: string[]): Promise<void> {
		return await exiftoolDelegate.write(file, tags, args);
	},
	end(gracefully?: boolean): Promise<void> {
		return exiftoolDelegate.end(gracefully);
	},
};

/**
 * Return all tag names that are writable by exiftool, as object
 * @type {Promise<object<boolean>>}
 */
export const writableTags: IBooleanMap = (() => {
	const tags = execFileSync(DefaultExifToolPath, ["-listw"], { encoding: "utf8" })
		.trim()
		.split(/\s+/);
	const result: IBooleanMap = {};
	tags.forEach((tagName) => {
		result[tagName] = true;
	});
	return result;
})();

export interface IBooleanMap {
	[key: string]: boolean;
}

/**
 * Copies all writable tags from the sourceFile to the targetFile
 * @param sourceFile
 * @param targetFile
 */
export async function copyTags(sourceFile: string, targetFile: string): Promise<void> {
	try {
		await exiftool.write(targetFile, {}, ["-TagsFromFile", sourceFile, "-all:all>all:all"]);
	} catch (e) {
		// Remove exiftools tmp_file, which seems to remain in case of an error
		await fs.remove(targetFile + "_exiftool_tmp");
		if (!e.message.match(/^Warning: No writable tags set from/)) {
			throw e;
		}
	}
}

export async function addCreationDateIfMissing(
	file: string,
	modificationDate: Date
): Promise<void> {
	try {
		await tryAddCreationDateIfMissing(file, modificationDate);
	} catch (error) {
		await repairTags(file);
		await tryAddCreationDateIfMissing(file, modificationDate);
	}
}

async function repairTags(file) {
	const tmpFile = replaceExtension(file, "." + cuid.slug() + ".tmp.jpg");
	await exiftoolDelegate.rewriteAllTags(file, tmpFile);
	await fs.move(tmpFile, file, { overwrite: true });
}

/**
 * Make sure that a creationDate of some kind is set as tag. If none is set, use value of "modificationDate"
 */
export async function tryAddCreationDateIfMissing(
	file: string,
	modificationDate: Date
): Promise<void> {
	const tags: Tags = await exiftool.read(file, [
		"-CreateDate",
		"-DateTimeOriginal",
		"-ModifyDate",
		"-TimeZone",
		"-GPSDateTime",
	]);
	let tagDate = tags.CreateDate || tags.DateTimeOriginal || tags.ModifyDate;
	if (tagDate == null || !(tagDate instanceof ExifDateTime)) {
		tagDate = exifDateTimeFromDate(modificationDate);
	}
	if (!tagDate.hasZone) {
		const maybeFixedTagDate = ExifDateTime.fromDateTime(tagDate.toDateTime().toLocal());
		if (maybeFixedTagDate != null) {
			tagDate = maybeFixedTagDate;
		}
	}

	const writeTags: WriteTags = {
		CreateDate: tagDate.toString(),
		DateTimeOriginal: tagDate.toString(),
	};

	// Add timezone information by adding a GPSDateTime,
	if (!tags.GPSDateTime) {
		writeTags.GPSDateTime = tagDate.toISOString({ includeOffset: true });
	}
	await exiftool.write(file, writeTags, ["-overwrite_original"]);
}

/**
 * Promisified version of "mmm.Magic#detectFile" with mime-type output
 * @param {string} file path to the checked file.
 * @return {Promise<string>} the mime-type of the file
 */
export async function detectMimeType(file: string): Promise<string> {
	// tslint:disable-next-line:no-bitwise
	await fs.access(file, fs.constants.F_OK | fs.constants.R_OK); // Only proceed if the file is readable
	try {
		const exiftoolData = await exiftool.read(file, ["-MIMEType"]);
		return exiftoolData.MIMEType || "application/octet-stream";
	} catch (error) {
		if (error.message.match(/^stderr\.data: (Unknown file type|File format error)/i)) {
			return "application/octet-stream";
		}
		throw error;
	}
}

/**
 * Convert a JavaScript-Date to an ExifDateTime object
 * @param date
 */
function exifDateTimeFromDate(date: Date) {
	return new ExifDateTime(
		date.getFullYear(),
		date.getMonth() + 1,
		date.getDate(),
		date.getHours(),
		date.getMinutes(),
		date.getSeconds(),
		date.getMilliseconds(),
		-date.getTimezoneOffset()
	);
}
