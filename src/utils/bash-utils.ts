/**
 * Quote an argument for a bash command, such that it is recognized as single parameter
 *
 * (replace quotes by escaped quotes and surround everything with quotes)
 */
export function bashQuote(cmdline: string[]): string {
	return cmdline
		.map((arg) => arg.replace(/'/g, "\\'"))
		.map((arg) => `'${arg}'`)
		.join(" ");
}
