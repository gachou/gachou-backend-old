import { PassThrough, Readable } from "stream";
import { streamToBuffer } from "./stream-to-buffer";

test("collects the bytes of a stream into a buffer", async () => {
	const stream = new PassThrough();
	const result = streamToBuffer(stream);
	stream.write(Buffer.from([1, 2, 3]));
	stream.write(Buffer.from([4, 5, 6]));
	stream.end(Buffer.from([7, 8, 9]));
	await expect(result).resolves.toEqual(Buffer.from([1, 2, 3, 4, 5, 6, 7, 8, 9]));
});

test("rejects on errors", async () => {
	const stream = new Readable({
		read() {
			this.destroy(new Error("test-error"));
		},
	});
	const result = streamToBuffer(stream);
	await expect(result).rejects.toThrow(/test-error/);
});
