import { compare } from "./compare-image";
import { temporaryDirectoryForTest } from "@gachou/testdata";

const tmpDir = temporaryDirectoryForTest(module);

jest.setTimeout(10000);

describe("The compare-image-tools", function () {
	beforeEach(() => tmpDir.refresh());

	describe("the 'compare'-function", function () {
		it("should return a high value for images with lots of differences", async function () {
			// TODO: use one of my own images to make sure there is no copyright
			expect(
				await compare(
					require.resolve("./fixtures/img1.jpg"),
					require.resolve("./fixtures/img2.jpg"),
					tmpDir.resolve("img1-2-high.png"),
					0.1
				)
			).toBeGreaterThan(9000);
		});

		it("should return a lower value a higher treshold", async function () {
			const compareResult = await compare(
				require.resolve("./fixtures/img1.jpg"),
				require.resolve("./fixtures/img2.jpg"),
				tmpDir.resolve("img1-2-high.png"),
				0.3
			);
			expect(compareResult).toBeGreaterThan(5000);
			expect(compareResult).toBeLessThan(8000);
		});

		it("should return a low value for images with few of differences", async function () {
			const compareResult = await compare(
				require.resolve("./fixtures/img1.jpg"),
				require.resolve("./fixtures/img3.jpg"),
				tmpDir.resolve("img1-3-low.png"),
				0.1
			);
			expect(compareResult).toBeGreaterThan(50);
			expect(compareResult).toBeLessThan(500);
		});

		it("should return a zero value for images with no differences", async function () {
			expect(
				await compare(
					require.resolve("./fixtures/img1.jpg"),
					require.resolve("./fixtures/img1.jpg"),
					tmpDir.resolve("img1-1-zero.png"),
					0.1
				)
			).toEqual(0);
		});
	});
});
