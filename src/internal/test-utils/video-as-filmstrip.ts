import cp from "child_process";
import ffmpeg from "ffmpeg-static";
import { path as ffprobePath } from "ffprobe-static";
import { PathLike } from "fs";

export async function videoAsFilmstrip(actualVideo: PathLike): Promise<Buffer> {
	// https://superuser.com/questions/625189/combine-multiple-images-to-form-a-strip-of-images-ffmpeg
	// Count frames in file https://stackoverflow.com/a/28376817/4251384
	const nrFrames = parseInt(
		cp.execFileSync(
			ffprobePath,
			([] as string[]).concat(
				["-v", "error"],
				["-select_streams", "v:0"],
				["-show_entries", "stream=nb_frames"],
				["-of", "default=nokey=1:noprint_wrappers=1"],
				[String(actualVideo)]
			)
		)
	);

	if (isNaN(nrFrames)) {
		throw new Error("Unable to determine number of frames in file " + actualVideo);
	}

	return cp.execFileSync(
		ffmpeg,
		([] as string[]).concat(
			["-i", String(actualVideo)],
			["-v", "error"],
			["-vf", `tile=1x${nrFrames}:padding=5:margin=5`],
			["-bitexact"],
			["-c:v", "png"],
			["-f", "image2pipe"],
			["pipe:1"]
		),
		{ encoding: "buffer", maxBuffer: 100 * 1024 * 1024 }
	);
}
