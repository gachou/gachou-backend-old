/* eslint-disable no-console */

let lastLogTime = Date.now();
export function millisPassed(): number {
	const lastLogTimeCopy = lastLogTime;
	const now = Date.now();
	try {
		return now - lastLogTimeCopy;
	} finally {
		lastLogTime = now;
	}
}

let logCounter = 0;

export function count(): number {
	return logCounter++;
}

export async function log<T>(name: string, callback: () => Promise<T>): Promise<T> {
	const c = count();
	console.log(name, c, "+" + millisPassed());
	try {
		return await callback();
	} finally {
		console.log(name + " - done", c, "+" + millisPassed());
	}
}
