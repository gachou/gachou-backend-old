import fs from "fs";
import jpegJs from "jpeg-js";
import { PNG } from "pngjs";
import pixelmatch from "pixelmatch";
import { Filename } from "../../api";

export function readFile(file: Filename): jpegJs.RawImageData<Buffer> {
	const data = fs.readFileSync(file);
	return jpegJs.decode(data);
}

export async function compare(
	expectedPath: string,
	actualPath: string,
	diffPath: string,
	threshold: number
): Promise<number> {
	const expected = readFile(expectedPath);
	const actual = readFile(actualPath);
	const diff = new PNG({ width: expected.width, height: expected.height });
	const result = pixelmatch(
		expected.data,
		actual.data,
		diff.data,
		expected.width,
		expected.height,
		{ threshold }
	);

	return new Promise<number>((resolve, reject) => {
		diff
			.pack()
			.pipe(fs.createWriteStream(diffPath))
			.on("finish", () => resolve(result))
			.on("error", (err) => reject(err));
	});
}
