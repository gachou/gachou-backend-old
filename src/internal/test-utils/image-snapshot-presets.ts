// This is the only place where an import of "jest-image-snapshot" should be
// necessary, because if is loaded in the "setupFilesBeforeEnv" hook and
// every test can just use it.
// eslint-disable-next-line no-restricted-imports
import { configureToMatchImageSnapshot } from "jest-image-snapshot";

export const toMatchImageSnapshot = configureToMatchImageSnapshot({});
expect.extend({ toMatchImageSnapshot });

export const toMatchFilmstripSnapshot = configureToMatchImageSnapshot({
	comparisonMethod: "ssim",
	failureThreshold: 0.01,
	failureThresholdType: "percent",
	customDiffConfig: {
		ssim: "fast",
	},
});
expect.extend({ toMatchFilmstripSnapshot });

declare global {
	// eslint-disable-next-line @typescript-eslint/no-namespace
	namespace jest {
		interface Matchers<R> {
			toMatchImageSnapshot(): R;
			toMatchFilmstripSnapshot(): R;
		}
	}
}
