import getPort from "get-port";

// TODO: Replace request-promise by "got"
import { server } from "../../server/server";
import { IGachouStatus } from "../../api";
import { loadFixture } from "@gachou/testdata";
import { Fixtures } from "@gachou/testdata/dist/generated";
import fs from "fs-extra";
import path from "path";
import createDebug from "debug";
import got from "got";
import FormData from "form-data";

const debug = createDebug("gachou-backend:test-server");

export interface ITestServer {
	/**
	 * The chosen server port
	 */
	readonly port: number;

	waitForBackgroundTasks(): Promise<void>;

	uploadTestFixture(args: UploadFixtureParams): Promise<{ statusCode: number; body: unknown }>;
	getJson<T>(path: string): Promise<T>;

	shutdown(): Promise<void>;
}

interface UploadFixtureParams {
	fileName: string;
	fixtureName: keyof Fixtures;
	modificationDate?: string;
}

export async function createTestServer(
	dataDir: string,
	mongoDbUrl: string,
	tmpDir: string
): Promise<ITestServer> {
	const port = await getPort();
	await fs.remove(dataDir);
	await fs.mkdirs(dataDir);
	const gachouServer = await server({
		mediaDir: path.join(dataDir, "media"),
		cacheDir: path.join(dataDir, "tmp"),
		port,
		mongoDbUrl: mongoDbUrl,
	});

	const client = got.extend({
		prefixUrl: `http://localhost:${port}/`,
	});

	async function getJson<T>(path: string): Promise<T> {
		const response = await client.get<T>(path, { responseType: "json" });
		return response.body;
	}

	async function backgroundTasksRunning(): Promise<boolean> {
		const currentStatus: IGachouStatus = await getJson<IGachouStatus>("status");
		return currentStatus.tasks.waiting > 0 || currentStatus.tasks.running > 0;
	}

	async function waitForBackgroundTasks(): Promise<void> {
		while (await backgroundTasksRunning()) {
			await new Promise((resolve) => setTimeout(resolve, 200));
		}
	}

	async function uploadTestFixture({
		fileName,
		fixtureName,
		modificationDate,
	}: UploadFixtureParams): Promise<{ statusCode: number; body: unknown }> {
		debug("Preparing upload file");
		const inputFile = await loadFixture(fixtureName).andCopyTo(tmpDir, fileName);

		debug("Posting file");
		const form = new FormData();
		form.append("file", fs.createReadStream(inputFile));
		if (modificationDate != null) {
			form.append("modificationDate", modificationDate);
		}

		const response = await client.post("files", {
			body: form,
			throwHttpErrors: false,
			responseType: "json",
		});

		debug("Waiting for background-jobs to finish");
		await waitForBackgroundTasks();
		debug("Background jobs done");
		return response;
	}

	return {
		port,
		getJson,
		waitForBackgroundTasks,
		uploadTestFixture,
		async shutdown() {
			return gachouServer.close();
		},
	};
}
