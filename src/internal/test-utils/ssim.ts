import { Ffmpeg, ffmpeg } from "../../utils/ffmpeg";

export interface ISSIMResult {
	y: number;
	u: number;
	v: number;
	all: number;
}

/**
 * Function that compares two videos
 *
 * https://ffmpeg.org/ffmpeg-filters.html#ssim
 * https://stackoverflow.com/questions/33541077/find-percentage-of-difference-between-two-videos-using-ffmpeg
 *
 * @param {string} originalFile
 * @param {string} modifiedFile
 * @return {Promise<ISSIMResult>}
 */
export async function ssim(originalFile: string, modifiedFile: string): Promise<ISSIMResult> {
	let ssimValues: ISSIMResult | null = null;
	const ffmpeg1: Ffmpeg = ffmpeg();
	await ffmpeg1
		.addArgs("-loglevel", "info")
		.from(originalFile)
		.from(modifiedFile)
		.addArgs("-lavfi")
		.addArgs("ssim")
		.addArgs("-f", "null")
		.on("stderr", (line) => {
			return line.replace(
				/^\[Parsed_ssim.*?\] SSIM Y:([\d.]+) .*? U:([\d.]+) .* V:([\d.]+) .* All:([\d.]+).*/,
				function (match, y, u, v, all) {
					ssimValues = { y: Number(y), u: Number(u), v: Number(v), all: Number(all) };
				}
			);
		})
		.writeTo("-");

	if (!ssimValues) {
		throw new Error("ISSIMResult line not found in output");
	}
	return ssimValues;
}
