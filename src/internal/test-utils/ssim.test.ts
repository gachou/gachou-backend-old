import { loadFixture, temporaryDirectoryForTest } from "@gachou/testdata";
import { ssim } from "./ssim";

const tmpDir = temporaryDirectoryForTest(module);

beforeEach(async () => tmpDir.refresh());

describe("The ssim() method", function () {
	it("should return 1 for identical videos", async function () {
		const tmpFile = tmpDir.resolve("video.mp4");
		await loadFixture("basic/1-video-streamable.mp4").andCopyTo(tmpFile);
		expect(await ssim(tmpFile, tmpFile)).toEqual({ y: 1, u: 1, v: 1, all: 1 });
	});
});
