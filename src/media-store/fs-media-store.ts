import cuid from "cuid";
import * as fs from "fs-extra";
import hasha from "hasha";
import path from "path";
import { Filename, IMediaResponse, IStoreApi, IStreamOptions } from "../api";
import { detectMimeType } from "../utils/exiftool-utils";
import { nameToPath, leftPadZero, iterateFiles } from "./file-utils";

/**
 * A media-store that loads files from the filesystem.
 */
export class FsMediaStore implements IStoreApi {
	/**
	 * Create a new file-based media-store
	 * @param {string} mediaDir the base directory in which the files are stored
	 * @param {string} thumbDir the base directory in which thumbnails are stored
	 * @param {string} trashDir the base directory in which deleted files are stored
	 */
	public static async create(
		mediaDir: string,
		thumbDir: string,
		trashDir: string
	): Promise<FsMediaStore> {
		await Promise.all([fs.mkdirs(mediaDir), fs.mkdirs(thumbDir), fs.mkdirs(trashDir)]);
		return new FsMediaStore(mediaDir, thumbDir, trashDir);
	}

	private readonly mediaDir: string;
	private readonly thumbDir: string;
	private readonly trashDir: string;

	constructor(mediaDir: string, thumbDir: string, trashDir: string) {
		this.mediaDir = mediaDir;
		this.thumbDir = thumbDir;
		this.trashDir = trashDir;
	}

	public async get(name: string): Promise<IMediaResponse> {
		const fullPath = path.join(this.mediaDir, nameToPath(name));
		return {
			file: () => Promise.resolve(fullPath),
			mimeType: await detectMimeType(fullPath),
			stream: (options?: IStreamOptions) =>
				Promise.resolve(
					fs.createReadStream(fullPath, { start: options?.start, end: options?.end })
				),
		};
	}

	/**
	 * Add a new image to the store. Compute the filename from the creationDate
	 * @param {string} fullPath
	 * @param {Date} creationDate
	 * @return {Promise<string>}
	 */
	public async add(fullPath: string, creationDate: Date): Promise<string> {
		const year = leftPadZero(creationDate.getFullYear(), 4);
		const month = leftPadZero(creationDate.getMonth() + 1);
		const day = leftPadZero(creationDate.getDate());
		const hour = leftPadZero(creationDate.getHours());
		const minute = leftPadZero(creationDate.getMinutes());
		const second = leftPadZero(creationDate.getSeconds());
		const hash = await hasha.fromFile(fullPath, { algorithm: "md5" });
		const shortHash = hash != null ? hash.slice(0, 10) : "";
		const [, baseName, extName] = path.basename(fullPath).match(/^(.*)\.([^.]*)$/) || [
			null,
			"",
			"",
		];

		const name = `${year}-${month}-${day}__${hour}-${minute}-${second}-${baseName}-${shortHash}.${extName}`;

		await fs.move(fullPath, path.join(this.mediaDir, nameToPath(name)), { overwrite: false });
		return name;
	}

	public async update(name: string, cb: (fullPath: Filename) => Promise<void>): Promise<void> {
		const fileInStore = path.join(this.mediaDir, nameToPath(name));
		const tmpFile = path.join(cuid() + "-" + path.basename(fileInStore));
		await fs.copy(fileInStore, tmpFile);
		try {
			await cb(tmpFile);
			await fs.move(tmpFile, fileInStore, { overwrite: true });
		} catch (err) {
			await fs.remove(tmpFile);
		}
	}

	public async moveToTrash(name: string): Promise<void> {
		const fileInStore = path.join(this.mediaDir, nameToPath(name));
		const fileInTrash = path.join(this.trashDir, nameToPath(name));
		await fs.move(fileInStore, fileInTrash);
	}

	public async restoreFromTrash(name: string): Promise<void> {
		const fileInTrash = path.join(this.trashDir, nameToPath(name));
		const fileInStore = path.join(this.mediaDir, nameToPath(name));
		await fs.move(fileInTrash, fileInStore);
	}

	public async *iterate(): AsyncGenerator<string> {
		for await (const files of iterateFiles(this.mediaDir)) {
			for (const file of files) {
				yield file;
			}
		}
	}

	public async count(): Promise<number> {
		let count = 0;
		for await (const files of iterateFiles(this.mediaDir)) {
			count += files.length;
		}
		return count;
	}

	/**
	 * Retrieve a thumbnail of the given size
	 * @param {string} name
	 * @param {string} spec
	 * @return {Promise<IMediaResponse>}
	 */
	public async getThumb(name: string, spec: string): Promise<IMediaResponse> {
		const fullPath = path.join(this.thumbDir, spec, nameToPath(name));
		return {
			file: () => Promise.resolve(fullPath),
			mimeType: await detectMimeType(fullPath),
			stream: (options?: IStreamOptions) => Promise.resolve(fs.createReadStream(fullPath, options)),
		};
	}

	public putThumb(name: string, spec: string, fullPath: string): Promise<void> {
		return fs.move(fullPath, path.join(this.thumbDir, spec, nameToPath(name)));
	}

	public async removeThumbnails(name: string): Promise<void> {
		const allSpecs = await fs.readdir(this.thumbDir);
		const deletionPromises = allSpecs.map(async (spec) =>
			fs.remove(path.join(this.thumbDir, spec, nameToPath(name)))
		);
		await Promise.all(deletionPromises);
	}
}
