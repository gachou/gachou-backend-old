import { temporaryDirectoryForTest } from "@gachou/testdata";
import { iterateFiles } from "./file-utils";
import fs from "fs-extra";
import * as path from "path";

const tmpDir = temporaryDirectoryForTest(module);

describe("iterateFiles", () => {
	beforeEach(async () => {
		await tmpDir.refresh();
	});

	it("iterates all files according to the 'year/month/file'-schema", async () => {
		await addTestFile(tmpDir.resolve("media", "2016", "08", "2016-08-test1.jpg"));
		await addTestFile(tmpDir.resolve("media", "2016", "08", "2016-08-test2.jpg"));
		await addTestFile(tmpDir.resolve("media", "2016", "10", "2016-10-test1.jpg"));
		await addTestFile(tmpDir.resolve("media", "2017", "09", "2017-09-test1.jpg"));

		const result: string[] = [];
		for await (const files of iterateFiles(tmpDir.resolve("media"))) {
			result.push(...files);
		}
		result.sort();
		expect(result).toEqual([
			"2016-08-test1.jpg",
			"2016-08-test2.jpg",
			"2016-10-test1.jpg",
			"2017-09-test1.jpg",
		]);
	});

	it("ignores non-numerical years and months", async () => {
		await addTestFile(tmpDir.resolve("media", "2016", "08", "2016-08-test1.jpg"));
		await addTestFile(
			tmpDir.resolve("media", "non-number-2016", "08", "non-number-2016-08-test1.jpg")
		);
		await addTestFile(
			tmpDir.resolve("media", "2016", "non-number-08", "2016-non-number-08-test1.jpg")
		);

		const result: string[] = [];
		for await (const files of iterateFiles(tmpDir.resolve("media"))) {
			result.push(...files);
		}
		result.sort();
		expect(result).toEqual(["2016-08-test1.jpg"]);
	});

	it("ignores files when expecting directories", async () => {
		await addTestFile(tmpDir.resolve("media", "2014", "08"));
		await addTestFile(tmpDir.resolve("media", "2015"));
		await addTestFile(tmpDir.resolve("media", "2016", "08", "2016-08-test1.jpg"));

		const result: string[] = [];
		for await (const files of iterateFiles(tmpDir.resolve("media"))) {
			result.push(...files);
		}
		result.sort();
		expect(result).toEqual(["2016-08-test1.jpg"]);
	});
});

async function addTestFile(fullPath: string): Promise<void> {
	await fs.mkdirp(path.dirname(fullPath));
	await fs.writeFile(fullPath, "test-content\n");
}
