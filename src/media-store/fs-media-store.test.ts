import fs from "fs-extra";
import path from "path";
import streamEqual from "stream-equal";
import { loadFixture, temporaryDirectoryForTest } from "@gachou/testdata";
import { FsMediaStore } from "./fs-media-store";
import { streamToBuffer } from "../internal/test-utils/stream-to-buffer";

const tmpDir = temporaryDirectoryForTest(module);
const mediaDir = tmpDir.resolve("media");
const thumbDir = tmpDir.resolve("thumbs");
const trashDir = tmpDir.resolve("trash");

function stored(...file: string[]): string {
	return path.join(mediaDir, ...file);
}

function trashed(...file: string[]): string {
	return path.join(trashDir, ...file);
}

describe("The media-store", function () {
	let store: FsMediaStore;

	beforeEach(async function () {
		await fs.remove(mediaDir);
		await fs.remove(thumbDir);
		await fs.remove(trashDir);
		store = await FsMediaStore.create(mediaDir, thumbDir, trashDir);
		await Promise.all([
			loadFixture("basic/1-video-streamable.mp4").andCopyTo(
				stored("2013", "03", "2013-03-10__10-10-10-eins.mp4")
			),
			loadFixture("basic/1-video-streamable.mp4").andCopyTo(
				stored("2013", "03", "2013-03-10__10-10-10-zwei.mp4")
			),
			loadFixture("basic/1-video-streamable.mp4").andCopyTo(
				stored("2013", "03", "2013-03-10__10-10-10-drei.mp4")
			),
			loadFixture("basic/1-video-streamable.mp4").andCopyTo(
				stored("2013", "03", "2013-03-10__10-10-10-vier.wrong.extension")
			),
		]);
	}, 10000);

	describe("the get-method", () => {
		it("should return an appropriate response for a file", async () => {
			const mediaResponse = await store.get("2013-03-10__10-10-10-eins.mp4");
			expect(await mediaResponse.file()).toBe(
				stored("2013", "03", "2013-03-10__10-10-10-eins.mp4")
			);
			expect(mediaResponse.mimeType).toBe("video/mp4");
			expect(
				await streamEqual(
					await mediaResponse.stream(),
					fs.createReadStream(stored("2013", "03", "2013-03-10__10-10-10-eins.mp4"))
				)
			).toBe(true);
		});

		it("should return the mime-type based on the file contents, not the extension", async () => {
			const mediaResponse = await store.get("2013-03-10__10-10-10-vier.wrong.extension");
			expect(mediaResponse.mimeType).toBe("video/mp4");
		});

		it("should return partial files if requested", async () => {
			const mediaResponse = await store.get("2013-03-10__10-10-10-eins.mp4");
			const stream = await mediaResponse.stream({ start: 2, end: 5 });

			await expect(streamToBuffer(stream)).resolves.toEqual(Buffer.from([0x00, 0x20, 0x66, 0x74]));
		});
	});

	describe("the moveToTrash-method", () => {
		it("should move a file to the trash directory", async () => {
			const contentsOfFile = await fs.readFile(
				stored("2013", "03", "2013-03-10__10-10-10-eins.mp4")
			);

			await store.moveToTrash("2013-03-10__10-10-10-eins.mp4");

			expect(fs.existsSync(stored("2013", "03", "2013-03-10__10-10-10-eins.mp4"))).toBe(false);
			expect(fs.existsSync(trashed("2013", "03", "2013-03-10__10-10-10-eins.mp4"))).toBe(true);
			const contentsOfTrashedFile = await fs.readFile(
				trashed("2013/03/2013-03-10__10-10-10-eins.mp4")
			);
			expect(contentsOfTrashedFile).toEqual(contentsOfFile);
		});
	});

	describe("the restoreFromTrash-method", () => {
		it("should move a file from the trash directory to the media dir", async () => {
			const trashedFile = trashed("2013", "03", "2013-03-10__10-10-10-trashed.mp4");
			await loadFixture("basic/1-video-streamable.mp4").andCopyTo(trashedFile);
			const contentsOfTrashedFile = await fs.readFile(trashedFile);

			await store.restoreFromTrash("2013-03-10__10-10-10-trashed.mp4");

			expect(fs.existsSync(trashed("2013", "03", "2013-03-10__10-10-10-trashed.mp4"))).toBe(false);
			expect(fs.existsSync(stored("2013", "03", "2013-03-10__10-10-10-trashed.mp4"))).toBe(true);
			const contentsOfRestoredFile = await fs.readFile(
				stored("2013", "03", "2013-03-10__10-10-10-trashed.mp4")
			);
			expect(contentsOfRestoredFile).toEqual(contentsOfTrashedFile);
		});
	});

	describe("the iterate-method", () => {
		it("returns all filenames in an async generator", async function () {
			const files: string[] = [];
			for await (const file of store.iterate()) {
				files.push(file);
			}
			files.sort();
			expect(files).toEqual([
				"2013-03-10__10-10-10-drei.mp4",
				"2013-03-10__10-10-10-eins.mp4",
				"2013-03-10__10-10-10-vier.wrong.extension",
				"2013-03-10__10-10-10-zwei.mp4",
			]);
		});
	});

	describe("the count-method", () => {
		it("returns the total number of files in the store", async function () {
			expect(await store.count()).toEqual(4);
		});
	});
});
