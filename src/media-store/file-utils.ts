import * as fs from "fs-extra";
import path from "path";

const VALID_FILENAME = /(\d{4})-(\d{2}).*/;

/**
 * Resolve the relative path of a file based on its name (id)
 * @param {string} name
 */
export function nameToPath(name: string): string {
	const match = name.match(VALID_FILENAME);
	if (!match) {
		throw new Error(`Invalid name "${name}". Expected YYYY-MM-something.ext`);
	}
	const [, year, month] = match;
	return `${year}/${month}/${name}`;
}

export function leftPadZero(n: number, length = 2): string {
	return ("0000" + n).substr(-length);
}

const NUMBER_REGEX = /^\d+$/;

export async function* iterateFiles(mediaDir: string): AsyncGenerator<string[]> {
	const years = await readDirIfPossible(mediaDir, (dir) => NUMBER_REGEX.test(dir));
	for (const year of years) {
		const yearDir = path.join(mediaDir, year);
		const months = await readDirIfPossible(yearDir, (dir) => NUMBER_REGEX.test(dir));
		for (const month of months) {
			const monthDir = path.join(mediaDir, year, month);
			yield readDirIfPossible(monthDir, (file) => file.startsWith(`${year}-${month}-`));
		}
	}
}

async function readDirIfPossible(
	directory: string,
	isRelevant: (name: string) => boolean
): Promise<string[]> {
	try {
		const entries = await fs.readdir(directory);
		return entries.filter(isRelevant);
	} catch (error) {
		if (error.code !== "ENOTDIR") {
			throw error;
		}
	}
	return [];
}
