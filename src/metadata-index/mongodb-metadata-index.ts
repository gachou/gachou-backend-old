import _ from "lodash";
import mongoose, { Connection, Document, Model, Schema } from "mongoose";
import {
	IExtents,
	IFacet,
	IMetadata,
	IMetadataEntry,
	IMetadataIndex,
	IMetadataQuery,
	IMetadataUpdate,
	ISearchResult,
} from "../api";
import { createLogger } from "../utils/logger";
import {
	isArrayOfType,
	isDateTime,
	isNumber,
	isObjectOfType,
	isOptional,
	isString,
	validateThat,
	withDefaultValue,
} from "../utils/typecheck";
import { DateTime } from "luxon";

const logger = createLogger("gachou:nedb-metadata-index");

/**
 * Definition for a facet value (name, label, and the way it is computed from the entry
 * @internal
 */
interface IFacetDefinition {
	label: string;
	compute: (entry: IMetadataEntry) => string;
	aggregation: (internalQuery: IInternalFacets) => Record<string, unknown>[];
}

const facetDefinitions: Record<string, IFacetDefinition> = {
	year: {
		compute: (entry: IMetadataEntry): string => entry.metadata.creationDate.toFormat("yyyy"),
		label: "Year",
		aggregation: (internalQuery) => [
			{
				$match: _.omit(internalQuery, ["facets.year"]),
			},
			{
				$group: {
					_id: "$facets.year",
					total: { $sum: 1 },
				},
			},
			{
				$sort: {
					_id: 1,
				},
			},
		],
	},
	month: {
		compute: (entry: IMetadataEntry): string => entry.metadata.creationDate.toFormat("LL"),
		label: "Month",
		aggregation: (internalQuery) => [
			{
				$match: _.omit(internalQuery, ["facets.month"]),
			},
			{
				$group: {
					_id: "$facets.month",
					total: { $sum: 1 },
				},
			},
			{
				$sort: {
					_id: 1,
				},
			},
		],
	},
	mimeType: {
		compute: (entry: IMetadataEntry): string => entry.metadata.mimeType,
		label: "Mime-Type",
		aggregation: (internalQuery) => [
			{
				$match: _.omit(internalQuery, ["facets.mimeType"]),
			},
			{
				$group: {
					_id: "$facets.mimeType",
					total: { $sum: 1 },
				},
			},
			{
				$sort: {
					_id: 1,
				},
			},
		],
	},
};

interface IInternalEntry {
	__lastUpdate__: Date;
	name: string;
	creationDate: Date;
	creationDateIso: string;
	height: number;
	width: number;
	lengthMs?: number;
	mimeType: string;
	rating?: number;
	tags: string[];
	facets: Record<string, unknown>;
}

const isMetadataEntry = isObjectOfType({
	name: isString,
	metadata: isObjectOfType<IMetadata>({
		creationDate: isDateTime,
		extents: isObjectOfType<IExtents>({
			height: isNumber,
			width: isNumber,
			lengthMs: isOptional(isNumber),
		}),
		mimeType: isString,
		rating: isOptional(isNumber),
		tags: withDefaultValue([], isArrayOfType(isString)),
	}),
});

const schema = new Schema<IInternalEntry>({
	name: {
		type: String,
		required: true,
	},
	__lastUpdate__: {
		type: Date,
		default: Date.now(),
	},
	creationDate: {
		type: Date,
		required: true,
	},
	creationDateIso: {
		type: String,
		required: true,
	},
	height: {
		type: Number,
		required: true,
	},
	width: {
		type: Number,
		required: true,
	},
	lengthMs: {
		type: Number,
		required: false,
	},
	mimeType: {
		type: String,
		required: true,
	},
	rating: {
		type: Number,
		required: false,
	},
	tags: {
		type: [String],
		required: false,
	},
	// TODO: simplify the next line, remove the function
	facets: mapValues(facetDefinitions, () => ({
		type: String,
		required: false,
	})),
});

function mapCoreToMongoModel(input: IMetadataEntry): IInternalEntry {
	return {
		__lastUpdate__: new Date(),
		...input.metadata.extents,
		mimeType: input.metadata.mimeType,
		name: input.name,
		tags: input.metadata.tags || [],
		rating: input.metadata.rating,
		creationDate: input.metadata.creationDate.toJSDate(),
		creationDateIso: input.metadata.creationDate.toISO(),
		facets: mapValues(facetDefinitions, (facetDefinition) => facetDefinition.compute(input)),
	};
}

function mapMongoModelToCore(input: IInternalEntry): IMetadataEntry {
	const result: IMetadataEntry = {
		name: input.name,
		metadata: {
			...input,
			creationDate: DateTime.fromISO(input.creationDateIso, { setZone: true }),
			extents: input,
		},
	};
	return validateThat(result, isMetadataEntry);
}

interface IInternalFacets {
	[id: string]: string | undefined;
}

function mapValues<T, R>(input: Record<string, T>, mapFn: (input: T) => R): Record<string, R> {
	const result: Record<string, R> = {};
	Object.keys(input).forEach((key) => {
		result[key] = mapFn(input[key]);
	});
	return result;
}

type AggregateResult = Record<string, { _id: string; total: number }[]>[];

export interface Options {
	username?: string;
	password?: string;
}

export class MongoDbMetadataIndex implements IMetadataIndex {
	private connection: Connection;

	public static async create(mongoDbUrl: string, options?: Options): Promise<MongoDbMetadataIndex> {
		const mongooseConfig = {
			useNewUrlParser: true,
			useUnifiedTopology: true,
			numberOfRetries: 100,
			...(options?.username && { user: options.username }),
			...(options?.password && { pass: options.password }),
		};

		const connection = await mongoose.createConnection(mongoDbUrl, mongooseConfig);
		return new MongoDbMetadataIndex(connection);
	}

	private MetadataModel: Model<Document & IInternalEntry>;

	private constructor(connection: Connection) {
		this.connection = connection;
		this.MetadataModel = connection.model("FileMetadata", schema);
	}

	public async add(entry: IMetadataEntry): Promise<void> {
		logger.info({ action: "add", entry });
		const internalEntry = mapCoreToMongoModel(entry);

		const result = await this.MetadataModel.updateOne({ name: entry.name }, internalEntry, {
			upsert: true,
		}).exec();

		if (result.numberOfUpdated === 0) {
			throw new Error("No entries updated");
		}
		logger.info({ action: "add", done: true, entry });
	}

	/**
	 * Update a file in the metadata-db
	 */
	public async update(_name: string, _metadata: IMetadataUpdate): Promise<void> {
		throw new Error("Metadata-Update Not yet implemented");
	}

	public async remove(file: string): Promise<void> {
		logger.info({ action: "remove", file });
		await this.MetadataModel.deleteOne({ name: file }).exec();
		logger.info({ action: "remove", done: true, file });
	}

	/**
	 * Perform a query on the metadata index
	 * @param query
	 * @param {number} start
	 * @param {number} limit
	 * @return {Promise<ISearchResult>}
	 */
	public async find(query: IMetadataQuery, start = 0, limit = 12): Promise<ISearchResult> {
		const internalQuery: IInternalFacets = _(query)
			.pickBy((value) => value != null)
			.mapKeys((value, key) => `facets.${key}`) // match with entry-structure
			.value();

		const results: IInternalEntry[] = await this.MetadataModel.find(internalQuery)
			.sort({ creationDate: -1 })
			.skip(start)
			.limit(limit)
			.lean()
			.exec();

		const total = await this.MetadataModel.countDocuments(internalQuery).exec();
		const facets = await this.computeFacets(internalQuery);
		return {
			context: {
				limit,
				start,
				total,
			},
			facets: facets,
			results: results.map((entry) => mapMongoModelToCore(entry)),
		};
	}

	private async computeFacets(internalQuery: IInternalFacets): Promise<IFacet[]> {
		const aggregateResult: AggregateResult = await this.MetadataModel.aggregate()
			.facet(
				mapValues(facetDefinitions, (facetDefinition) => facetDefinition.aggregation(internalQuery))
			)
			.exec();

		return Object.keys(aggregateResult[0]).map((key) => {
			return {
				id: key,
				label: facetDefinitions[key].label,
				values: aggregateResult[0][key].map((bucket) => {
					const value = bucket._id;
					return {
						value: value,
						count: bucket.total,
						active: internalQuery["facets." + key] === value,
					};
				}),
			};
		});
	}

	/**
	 * Return metadata of the specified file
	 * @param {string} name the name of the file
	 * @return {Promise<IMetadataEntry | null>}
	 */
	public async read(name: string): Promise<IMetadataEntry | null> {
		const result: IInternalEntry[] = await this.MetadataModel.find({ name: name })
			.limit(1)
			.lean()
			.exec();
		return result.length > 0 ? mapMongoModelToCore(result[0]) : null;
	}

	/**
	 *
	 * @param {number} syncId the timestamp of the start of the sync
	 * @return {Promise<void>}
	 */
	public async syncFinished(syncId: number): Promise<void> {
		// Remove all metadata that has not been touched during the sync
		await this.MetadataModel.deleteMany({
			__lastUpdate__: {
				$lt: new Date(syncId),
			},
		});
	}

	/**
	 * Return the current timestamp as number
	 * @return {Promise<number>}
	 */
	public async syncStarted(): Promise<number> {
		return Date.now();
	}

	public async shutdown(): Promise<void> {
		await this.connection.close();
	}
}
