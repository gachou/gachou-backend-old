import { MongoDbMetadataIndex } from "./mongodb-metadata-index";
import { DateTime } from "luxon";
import { IMetadataEntry } from "../api";
import { useMongoDbTestInstance } from "../../test-setup/mongodb-test-instance";

describe("The nedb-metadata-index:", () => {
	let metadataIndex: MongoDbMetadataIndex;

	const image201801: IMetadataEntry = {
		metadata: {
			creationDate: DateTime.fromISO("2018-01-19T13:45:23+0100", { setZone: true }),
			mimeType: "image/jpeg",
			extents: {
				width: 300,
				height: 200,
			},
			tags: [],
		},
		name: "aFile1.jpeg",
	};

	const image201802 = {
		metadata: {
			creationDate: DateTime.fromISO("2018-02-19T13:45:23+0100", { setZone: true }),
			mimeType: "image/jpeg",
			extents: {
				width: 300,
				height: 200,
			},
			tags: [],
		},
		name: "aFile2.jpeg",
	};

	const video201702 = {
		metadata: {
			creationDate: DateTime.fromISO("2017-02-19T13:45:23+0100", { setZone: true }),
			mimeType: "video/mp4",
			extents: {
				width: 300,
				height: 200,
				lengthMs: 20000,
			},
			tags: [],
		},
		name: "aFile3.mp4",
	};

	const { clearDatabase, mongoDbUrl } = useMongoDbTestInstance();

	beforeEach(async () => {
		await clearDatabase();
		metadataIndex = await MongoDbMetadataIndex.create(mongoDbUrl());
		await metadataIndex.add(image201801);
		await metadataIndex.add(image201802);
		await metadataIndex.add(video201702);
	});

	afterEach(async () => {
		// Shut down to make sure that no intervals are still active (this would
		// prevent to process from quitting
		await metadataIndex.shutdown();
	});

	it("should find added documents", async () => {
		const target = await metadataIndex.find({});
		expect(target.results).toEqual([image201802, image201801, video201702]);
	});

	it("should read added documents by their name", async () => {
		const target = await metadataIndex.read(video201702.name);
		expect(target).toEqual(video201702);
	});

	it("should return null when trying to read non-existing documents", async () => {
		const target = await metadataIndex.read("non-existing-doc");
		expect(target).toBeNull();
	});

	it("should not find removed documents", async () => {
		await metadataIndex.remove("aFile3.mp4");
		const target = await metadataIndex.find({});
		expect(target.results).toEqual([image201802, image201801]);
	});

	it("should be able to filter the result list by month", async () => {
		const target = await metadataIndex.find({ month: "02" });
		expect(target.results).toEqual([image201802, video201702]);
	});

	it("should be able to filter the result list by year", async () => {
		const target = await metadataIndex.find({ year: "2018" });
		expect(target.results).toEqual([image201802, image201801]);
	});

	it("should be able to filter by multiple facets", async () => {
		const target = await metadataIndex.find({ year: "2018", month: "02" });
		expect(target.results).toEqual([image201802]);
	});

	it("should return facets for months, years and mime-type", async () => {
		const target = await metadataIndex.find({});
		expect(target.facets.find((f) => f.id === "year")).toEqual({
			id: "year",
			label: "Year",
			values: [
				{ active: false, count: 1, value: "2017" },
				{ active: false, count: 2, value: "2018" },
			],
		});
		expect(target.facets.find((f) => f.id === "month")).toEqual({
			id: "month",
			label: "Month",
			values: [
				{ active: false, count: 1, value: "01" },
				{ active: false, count: 2, value: "02" },
			],
		});
		expect(target.facets.find((f) => f.id === "mimeType")).toEqual({
			id: "mimeType",
			label: "Mime-Type",
			values: [
				{ active: false, count: 2, value: "image/jpeg" },
				{ active: false, count: 1, value: "video/mp4" },
			],
		});
	});

	it("should return the correct count and active flag for facets in filtered search", async () => {
		const target = await metadataIndex.find({ year: "2018" });
		expect(target.facets.find((f) => f.id === "year")).toEqual({
			id: "year",
			label: "Year",
			values: [
				{ active: false, count: 1, value: "2017" },
				{ active: true, count: 2, value: "2018" },
			],
		});
		expect(target.facets.find((f) => f.id === "month")).toEqual({
			id: "month",
			label: "Month",
			values: [
				{ active: false, count: 1, value: "01" },
				{ active: false, count: 1, value: "02" },
			],
		});
		expect(target.facets.find((f) => f.id === "mimeType")).toEqual({
			id: "mimeType",
			label: "Mime-Type",
			values: [{ active: false, count: 2, value: "image/jpeg" }],
		});
	});

	it("should adapt count and active flag for searches with multiple filters", async () => {
		const target = await metadataIndex.find({ year: "2018", month: "02" });
		expect(target.facets.find((f) => f.id === "year")).toEqual({
			id: "year",
			label: "Year",
			values: [
				{ active: false, count: 1, value: "2017" },
				{ active: true, count: 1, value: "2018" },
			],
		});
		expect(target.facets.find((f) => f.id === "month")).toEqual({
			id: "month",
			label: "Month",
			values: [
				{ active: false, count: 1, value: "01" },
				{ active: true, count: 1, value: "02" },
			],
		});
		expect(target.facets.find((f) => f.id === "mimeType")).toEqual({
			id: "mimeType",
			label: "Mime-Type",
			values: [{ active: false, count: 1, value: "image/jpeg" }],
		});
	});
});
