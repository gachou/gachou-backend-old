import { exiftool, Tags, WriteTags } from "../utils/exiftool-utils";
import { IMetadata, IMetadataHandler } from "../api";
import { firstNonNull, parseDate, secondsToMillis } from "./converter";
import { pick } from "lodash";

export class ExiftoolMetadataHandler implements IMetadataHandler {
	public async extractMetadata(file: string): Promise<IMetadata> {
		const tags: Tags = await exiftool.read(file, ["-n"]);
		return {
			creationDate: firstNonNull(
				parseDate(tags.DateTimeOriginal),
				parseDate(tags.CreateDate)
			).orThrow(
				"Unable to determine creationDate from " +
					JSON.stringify(pick(tags, ["DateTimeOriginal", "CreateDate"]))
			),
			mimeType: tags.MIMEType || "application/data",
			extents: {
				width: firstNonNull(tags.ImageWidth).orThrow(
					"Unable to determine width for image %s",
					file
				),
				height: firstNonNull(tags.ImageHeight).orThrow(
					"Unable to determine width for image %s",
					file
				),
				lengthMs: secondsToMillis(tags.Duration),
			},
			tags: tags.HierarchicalSubject || [],
		};
	}

	public async updateMetadata(file: string, metadata: Partial<IMetadata>): Promise<void> {
		const tags: WriteTags = {};
		if (metadata.creationDate) {
			tags.AllDates = metadata.creationDate.toISO();
			tags.GPSDateTime = metadata.creationDate.toUTC().toISO();
			tags.TimeZoneOffset = metadata.creationDate.offset;
		}
		return exiftool.write(file, tags);
	}
}
