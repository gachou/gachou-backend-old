import { ExifDateTime } from "../utils/exiftool-utils";
import { format } from "util";
import { DateTime } from "luxon";

export function parseDate(value?: ExifDateTime | string): DateTime | undefined {
	if (value != null && typeof value !== "string") {
		return value.toDateTime();
	}
}

/**
 * Convert a duration into the number of milliseconds
 */
export function secondsToMillis(value?: number): number | undefined {
	if (value != null) {
		return Math.round(value * 1000);
	}
}

export function firstNonNull<T>(...values: Array<T | null | undefined>): Optional<T> {
	for (const val of values) {
		if (val != null) {
			return new Optional(val);
		}
	}
	return Optional.absent;
}

export class Optional<T> {
	public static absent: Optional<never> = new Optional<never>();

	private readonly value?: T;

	constructor(value?: T) {
		this.value = value;
	}

	public orThrow(message: string, ...params: unknown[]): T {
		if (this.value) {
			return this.value;
		}
		throw new Error(format(message, params));
	}
}
