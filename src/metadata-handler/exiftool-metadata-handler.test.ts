import fs from "fs-extra";

import { exiftool } from "../utils/exiftool-utils";
// TODO: Replace fixture-images by something from gachou-testdata
import images from "fixture-images";
import { loadFixture, temporaryDirectoryForTest } from "@gachou/testdata";
import { ExiftoolMetadataHandler } from "./exiftool-metadata-handler";
import { DateTime } from "luxon";

const tmpDir = temporaryDirectoryForTest(module);

describe("The exiftool-metadata-handler", () => {
	let handler: ExiftoolMetadataHandler;

	beforeEach(async () => {
		await tmpDir.refresh();
		handler = new ExiftoolMetadataHandler();
	});

	it("should read metadata tags from mp4-videos", async function () {
		const tmpFile = tmpDir.resolve("video.mp4");
		await loadFixture("basic/1-video-streamable.mp4").andCopyTo(tmpFile);
		await exiftool.write(tmpFile, {
			AllDates: "2017-07-27T10:28:35.000Z",
			HierarchicalSubject: ["Test/Video"],
		});
		expect(await handler.extractMetadata(tmpFile)).toEqual({
			creationDate: DateTime.fromISO("2017-07-27T10:28:35.000Z", { setZone: true }),
			mimeType: "video/mp4",
			extents: {
				height: 576,
				lengthMs: 2043,
				width: 1024,
			},
			tags: ["Test/Video"],
		});
	});

	it("should read metadata tags from jpg-images deriving the timezone", async function () {
		const file = tmpDir.resolve("image.jpg");
		await fs.writeFile(file, images.still.jpg);
		await exiftool.write(file, {
			AllDates: "2017-07-27 10:28:35",
			GPSDateTime: "2017-07-27 14:28:35", // GPSDateTime is used to derived timezone
		});
		expect(await handler.extractMetadata(file)).toEqual({
			creationDate: DateTime.fromISO("2017-07-27T10:28:35.000-0400", { setZone: true }),
			mimeType: "image/jpeg",
			extents: {
				height: 192,
				lengthMs: undefined,
				width: 256,
			},
			tags: [],
		});
	});
});
