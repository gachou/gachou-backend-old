#!/usr/bin/env node

import yargs, { Argv } from "yargs";
import { server } from "./server/server";
import { createLogger } from "./utils/logger";
import * as fs from "fs";

const logger = createLogger("gachou-backend:server-cli");

const typedYargs: Argv = yargs;
const argv = typedYargs
	.config("config", "path to a config file", (file) => JSON.parse(fs.readFileSync(file, "utf-8")))
	.env("GACHOU_CONFIG")
	.option("port", {
		default: 5000,
		describe: "port to bind on",
		type: "number",
	})
	.option("host", {
		default: "127.0.0.1",
		describe: "the address to bind on",
		type: "string",
	})
	.option("mediaDir", {
		describe: "base directory containing the media files",
		type: "string",
	})
	.option("cacheDir", {
		describe: "base directory for the thumbnails",
		type: "string",
	})
	.option("staticDir", {
		describe: "baseDirectory containing assets to serve for everything other than /backend",
		type: "string",
	})
	.option("mongoDbUrl", {
		describe: "connect-url of the mongo-db-instance used to store metadata and other things.",
		type: "string",
	})
	.option("mongoDbUsername", {
		describe: "username to use when connecting to the mongodb-instance.",
		type: "string",
	})
	.option("mongoDbPassword", {
		describe: "password to use when connecting to the mongodb-instance.",
		type: "string",
	})
	.demandOption(["mediaDir", "cacheDir", "mongoDbUrl"])
	.parse();

server(argv).then(
	(gachouServer) => logger.info({ address: gachouServer.address }, "Server listening"),
	(err: Error) => {
		logger.error(err);
		process.exit(1);
	}
);
