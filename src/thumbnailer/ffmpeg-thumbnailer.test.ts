import { exiftool, Tags } from "../utils/exiftool-utils";
import { loadFixture, temporaryDirectoryForTest } from "@gachou/testdata";
import { FfmpegThumbnailer } from "./ffmpeg-thumbnailer";
import { Fixtures } from "@gachou/testdata/dist/generated";
import { videoAsFilmstrip } from "../internal/test-utils/video-as-filmstrip";

const tmpDir = temporaryDirectoryForTest(module);

jest.setTimeout(10000);

describe("The ffmpeg-thumbnailer", function () {
	beforeEach(async () => tmpDir.refresh());

	describe("the 'createThumbnail'-method", function () {
		it("filmstrip of input file for comparison", async function () {
			const tmpFile = tmpDir.resolve("video.mp4");
			await loadFixture("basic/123.mp4").andCopyTo(tmpFile);
			const actualFilmstrip = await videoAsFilmstrip(tmpFile);
			expect(actualFilmstrip).toMatchImageSnapshot({});
		});

		it('should scale a video to the given size, with "crop: true"', async function () {
			const thumbnail = await scaleVideo("basic/123.mp4", { height: 300, width: 300, crop: true });
			const actualFilmstrip = await videoAsFilmstrip(thumbnail);
			expect(actualFilmstrip).toMatchFilmstripSnapshot();
		});

		it('should scale a video to the given size, with "crop: false"', async function () {
			const thumbnail = await scaleVideo("basic/123.mp4", { height: 300, width: 300, crop: false });
			const actualFilmstrip = await videoAsFilmstrip(thumbnail);
			expect(actualFilmstrip).toMatchFilmstripSnapshot();
		}, 20000);

		it("removes exif tags", async () => {
			const thumbnail = await scaleVideo("basic/123.mp4", { height: 300, width: 300, crop: false });
			expect(thumbnail).toBe(tmpDir.resolve("video-thumbnail.mp4"));
			const tags: Tags = await exiftool.read(thumbnail);
			expect(tags.HierarchicalSubject).toBe(undefined);
		});
	});
});

async function scaleVideo(
	fixtureName: keyof Fixtures,
	spec: { width: number; crop: boolean; height: number }
) {
	const tmpFile = tmpDir.resolve("video.mp4");
	await loadFixture(fixtureName).andCopyTo(tmpFile);
	await exiftool.write(tmpFile, { HierarchicalSubject: "abc/bcd/cde" });

	const targetFile = tmpDir.resolve("video-thumbnail");
	return await new FfmpegThumbnailer().createThumbnail(tmpFile, spec, targetFile);
}
