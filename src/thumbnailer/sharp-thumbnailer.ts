import path from "path";
import sharp from "sharp";
import { crop } from "smartcrop-sharp";
import { Filename, IParsedScaleSpec, IThumbnailer } from "../api";
const JPEG_OPTIONS = {
	quality: 50,
	progressive: true,
};

/**
 * This class uses `sharp` and `smartcrop-sharp` to create thumbnails of images
 */
export class SharpThumbnailer implements IThumbnailer {
	public async bid(file: string, mimeType: string): Promise<number> {
		return mimeType.match(/^image\/(jpeg|png)$/) ? 0 : -1;
	}

	public async createThumbnail(
		originalFile: Filename,
		spec: IParsedScaleSpec,
		thumbFilePrefix: string
	): Promise<Filename> {
		sharp.cache(false);
		const targetFile = thumbFilePrefix + path.extname(originalFile);
		if (spec.crop) {
			const rotatedImage = await sharp(originalFile).rotate().png().toBuffer();
			const result = await crop(rotatedImage, spec);
			await sharp(rotatedImage)
				.extract({
					height: result.topCrop.height,
					left: result.topCrop.x,
					top: result.topCrop.y,
					width: result.topCrop.width,
				})
				.jpeg(JPEG_OPTIONS)
				.resize(spec.width, spec.height, { fit: "inside" })
				.toFile(targetFile);
			return targetFile;
		} else {
			await sharp(originalFile)
				.rotate()
				.resize(spec.width, spec.height, { fit: "inside" })
				.jpeg(JPEG_OPTIONS)
				.toFile(targetFile);
			return targetFile;
		}
	}
}
