import fs from "fs-extra";

import { exiftool, Tags } from "../utils/exiftool-utils";
import fixtureImages from "fixture-images";
import { SharpThumbnailer } from "./sharp-thumbnailer";
import { IParsedScaleSpec } from "../api";
import { loadFixture, temporaryDirectoryForTest } from "@gachou/testdata";
import sharp from "sharp";
import cuid from "cuid";
import "trace";

const tmpDir = temporaryDirectoryForTest(module);

jest.setTimeout(10000);

describe("The sharp-thumbnailer", function () {
	beforeEach(() => tmpDir.refresh());

	describe("the 'createThumbnail'-method", function () {
		it("removes metadata from the image", async function () {
			const sourceFile = tmpDir.resolve("image-" + cuid.slug() + ".jpg");
			const thumbFilePrefix = tmpDir.resolve("image-thumbnail" + cuid());
			await fs.copy(fixtureImages.path.still.jpg, sourceFile);
			await exiftool.write(sourceFile, { HierarchicalSubject: "abc/bcd/cde" });

			const thumbFile = await new SharpThumbnailer().createThumbnail(
				sourceFile,
				{
					height: 300,
					width: 100,
					crop: true,
				},
				thumbFilePrefix
			);
			const tags: Tags = await exiftool.read(thumbFile);
			expect(tags.HierarchicalSubject).toBeUndefined();
		});

		it("should scale an image to the given size, cropping to an interesting part", async function () {
			const sourceFile = tmpDir.resolve("image-" + cuid.slug() + ".jpg");
			await fs.copy(fixtureImages.path.still.jpg, sourceFile);

			await scaleAndCompare(sourceFile, {
				height: 300,
				width: 100,
				crop: true,
			});
		});

		it("should scale an image to the given size, fitting it to a box", async function () {
			const sourceFile = tmpDir.resolve("image-" + cuid.slug() + ".jpg");
			await fs.copy(fixtureImages.path.still.jpg, sourceFile);
			await scaleAndCompare(sourceFile, {
				height: 300,
				width: 100,
				crop: false,
			});
		});

		it("rotates the image based on exif tags", async () => {
			const sourceFile = await loadFixture("basic/rotated-image.jpg").andCopyTo(
				tmpDir.resolve("image-" + cuid.slug() + ".jpg")
			);
			await scaleAndCompare(sourceFile, {
				height: 300,
				width: 100,
				crop: false,
			});
		});

		it("zooms to the correct area when cropping and rotating", async () => {
			const sourceFile = await loadFixture("basic/rotated-image.jpg").andCopyTo(
				tmpDir.resolve("image-" + cuid.slug() + ".jpg")
			);
			await scaleAndCompare(sourceFile, {
				height: 300,
				width: 100,
				crop: true,
			});
		});
	});
});

async function scaleAndCompare(sourceFile: string, scaleSpec: IParsedScaleSpec) {
	const thumbFilePrefix = tmpDir.resolve("image-thumbnail-" + cuid.slug());

	const thumbFile = await new SharpThumbnailer().createThumbnail(
		sourceFile,
		scaleSpec,
		thumbFilePrefix
	);
	expect(thumbFile).toBe(thumbFilePrefix + ".jpg");

	const pngContents = await sharp(thumbFile).png().toBuffer();
	expect(pngContents).toMatchImageSnapshot();
}
