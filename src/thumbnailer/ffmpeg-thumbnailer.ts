import Debug from "debug";
import fs from "fs-extra";
import path from "path";
import { Filename, IParsedScaleSpec, IThumbnailer } from "../api";
import { ffmpeg } from "../utils/ffmpeg";
import { dim, Dimensions } from "../utils/dimensions";
import { ffprobe } from "../utils/ffprobe";

const debug = Debug("gachou-backend:ffmpeg-thumbnailer");

export class FfmpegThumbnailer implements IThumbnailer {
	public async bid(file: string, mimeType: string): Promise<number> {
		return mimeType === "video/mp4" ? 0 : -1;
	}

	public async createThumbnail(
		originalFile: Filename,
		spec: IParsedScaleSpec,
		thumbFilePrefix: string
	): Promise<Filename> {
		const tmpFile = thumbFilePrefix + ".tmp" + path.extname(originalFile);
		const thumbFile = thumbFilePrefix + path.extname(originalFile);
		const transformFile = `${thumbFilePrefix}.trf`;
		try {
			// If this image should not be cropped, we need to compute new target sizes,
			// This is partly due to the fact that the target scales for ffmpeg must be divisible by 2
			// In this case, we compute the thumbnail slightly larger and crop the one surplus row or column
			const probe = await ffprobe(originalFile);
			const videoStream = probe.streams.find((stream) => stream.codec_type === "video");
			if (videoStream == null) {
				throw new Error(`File ${originalFile} does not have a video stream`);
			}

			const duration = probe.format.duration;
			const { width, height } = videoStream;

			const targetDimensions: Dimensions = spec.crop
				? dim(spec.width, spec.height)
				: dim(width, height).fitToBox(spec);

			const desiredNrOfFrames = 5;
			const frameRate = 2;

			await ffmpeg()
				.from(originalFile)
				.vf(
					// less images, faster playback
					`setpts=PTS/${duration / desiredNrOfFrames}`,
					"fps=" + frameRate,
					// scale down to outer box
					`scale=${targetDimensions
						.upToEvenNumbers()
						.format("w=%w:h=%h")}:force_original_aspect_ratio=increase`,
					`crop=${targetDimensions.format("%w:%h")}`
				)
				// do not duplicate frames https://ffmpeg.org/ffmpeg-filters.html#Examples-112
				.addArgs("-preset", "faster")
				.addArgs("-aspect", targetDimensions.format("%w:%h"))
				.addArgs("-movflags", "+faststart")
				.addArgs("-an") // mute audio
				.addArgs("-bitexact")
				.writeTo(thumbFile);

			// await ffmpeg().from(tmpFile).vf(`vidstabdetect=result=${transformFile}`).writeTo(thumbFile);
			// await ffmpeg().from(tmpFile).vf(`vidstabtransform=input=${transformFile}`).writeTo(thumbFile);

			return thumbFile;
		} finally {
			await Promise.all([fs.remove(tmpFile), fs.remove(transformFile)]).catch(debug);
		}
	}
}
