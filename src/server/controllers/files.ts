import { ICoreApi, ScaleSpec } from "../../api";
import { FastifyPluginAsync, FastifyRequest } from "fastify";
import multer from "fastify-multer";
import path from "path";
import { InvalidParametersError } from "../../utils/error";
import { NameInPath } from "./types";
import { File, FilesObject } from "fastify-multer/lib/interfaces";

type UploadRequestBody = {
	modificationDate: string;
};

type FilesInRequest = FilesObject | Partial<File>[];

interface MultipartFastifyRequest extends FastifyRequest {
	isMultipart(): boolean;
	file: File;
	files: FilesInRequest;
}

export function filesController(coreApi: ICoreApi, tmpDir: string): FastifyPluginAsync {
	return async (fastify) => {
		const upload = multer({ dest: path.join(tmpDir, "uploads") });

		fastify.post<{ Body: UploadRequestBody }>(
			"/files",
			{
				preHandler: upload.single("file"),
			},
			async (request) => {
				const modificationDate: Date = new Date(request.body.modificationDate);

				if (isNaN(modificationDate.getTime())) {
					throw new InvalidParametersError("Invalid parameter value", [
						{
							message: "value is not a valid ISO-DateTime",
							paramName: "modificationDate",
							value: request.body.modificationDate || null,
						},
					]);
				}

				const uploadedFilePath = (request as MultipartFastifyRequest).file.path;
				if (uploadedFilePath == null) {
					throw new Error("File upload failed, 'path' is missing.");
				}
				coreApi
					.addFile(uploadedFilePath, modificationDate)
					.catch((err) => request.log.error(err.message + "\n" + err.stack));
				return { success: true };
			}
		);

		fastify.delete<NameInPath>("/files/:name", async (request) => {
			const name: string = request.params.name;
			await coreApi.moveFileToTrash(name);
			return { success: true };
		});

		fastify.get<NameInPath & { Querystring: { download?: boolean; size?: ScaleSpec } }>(
			"/files/:name",
			async (request, reply) => {
				const name: string = request.params.name;
				const scaleSpec = request.query.size;
				const mediaResponse = scaleSpec
					? await coreApi.getThumbnail(name, scaleSpec)
					: await coreApi.getOriginalMedium(name);

				const stream = await mediaResponse.stream();
				if (request.query.download) {
					return reply
						.header("content-disposition", `attachment; filename="${request.params.name}"`)
						.header("content-type", mediaResponse.mimeType)
						.send(stream);
				}
				return reply
					.header("content-disposition", `inline`)
					.header("content-type", mediaResponse.mimeType)
					.send(stream);
			}
		);
	};
}
