import { ICoreApi } from "../../api";
import { FastifyPluginAsync } from "fastify";
import { HttpError } from "../../utils/error";
import { NameInPath } from "./types";

export function trashController(coreApi: ICoreApi): FastifyPluginAsync {
	return async (fastify) => {
		fastify.delete<NameInPath>("/trash/:name", async () => {
			// Delete file permanently
			throw new HttpError(400, "Not yet implemented");
		});

		fastify.patch<NameInPath & { Body: { action: "restore" } }>("/trash/:name", async (request) => {
			const name: string = request.params.name;
			await coreApi.restoreFileFromTrash(name);
			return { success: true };
		});
	};
}
