import { FastifyPluginAsync } from "fastify";
import { ICoreApi } from "../../api";
import { components } from "../gachou.openapi";

export function statusController(coreApi: ICoreApi): FastifyPluginAsync {
	return async (fastify) => {
		fastify.get(
			"/status",
			async (): Promise<components["schemas"]["Status"]> => {
				const status = coreApi.status();

				const fullUpdateStatus: components["schemas"]["Status"]["fullUpdateStatus"] = status.fullUpdateStatus
					? {
							...status.fullUpdateStatus,
							lastStarted: status.fullUpdateStatus?.lastStarted?.toISOString(),
							lastFinished: status.fullUpdateStatus?.lastFinished?.toISOString(),
					  }
					: undefined;

				return {
					...status,
					fullUpdateStatus,
				};
			}
		);
	};
}
