import { ICoreApi } from "../../api";
import { FastifyPluginAsync } from "fastify";
import { NameInPath } from "./types";

interface SearchQuery {
	start: number;
	limit: number;
	year: string;
	month: string;
	mimeType: string;
}

export function metadataController(coreApi: ICoreApi): FastifyPluginAsync {
	return async (fastify) => {
		fastify.get<{ Querystring: SearchQuery }>("/metadata", async (request) => {
			const start = Number(request.query.start);
			const limit = Number(request.query.limit);
			const { year, month, mimeType } = request.query;
			return coreApi.find({ year, month, mimeType }, start, limit);
		});

		fastify.get<NameInPath>("/metadata/:name", async (request) => {
			const name: string = request.params.name;
			return coreApi.getMetadata(name);
		});

		fastify.put<{ Body: { action: "full-update" } }>(
			"/metadata",
			{
				schema: {
					body: {
						properties: {
							action: {
								type: "string",
								enum: ["full-update"],
							},
						},
					},
				},
			},
			async (request) => {
				// Re-index
				coreApi.updateMetadataIndex().catch(request.log.error);
				return { success: "true" };
			}
		);
	};
}
