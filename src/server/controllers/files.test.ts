import createFastify, { FastifyInstance } from "fastify";
import http from "http";
import {
	IGachouStatus,
	IMediaFile,
	IMediaResponse,
	IMetadataEntry,
	IMetadataQuery,
	ISearchResult,
	ScaleSpec,
} from "../../api";
import { filesController } from "./files";
import { temporaryDirectoryForTest } from "@gachou/testdata";
import got from "got";
import fixtureImages from "fixture-images";
import fs from "fs";
import getPort from "get-port";

const gachouCore = {
	addFile: jest.fn<Promise<IMediaFile>, [string, Date]>(),
	moveFileToTrash: jest.fn<Promise<void>, [string]>(),
	restoreFileFromTrash: jest.fn<Promise<void>, [string]>(),
	updateMetadataIndex: jest.fn<Promise<void>, []>(),
	status: jest.fn<IGachouStatus, []>(),
	getOriginalMedium: jest.fn<Promise<IMediaResponse>, [string]>(),
	getThumbnail: jest.fn<Promise<IMediaResponse>, [string, ScaleSpec]>(),
	find: jest.fn<Promise<ISearchResult>, [IMetadataQuery, number, number]>(),
	getMetadata: jest.fn<Promise<IMetadataEntry>, [string]>(),
	shutdown: jest.fn<Promise<void>, []>(),
};
const tmpDir = temporaryDirectoryForTest(module);
let fastifyInstance: FastifyInstance;
let port: number;

beforeEach(async () => {
	jest.resetAllMocks();
	await tmpDir.refresh();
	fastifyInstance = createFastify<http.Server>();
	fastifyInstance.setErrorHandler((error, request, reply) => {
		// eslint-disable-next-line no-console
		console.error(error.stack);
		reply.status(error.statusCode || 500).send(error.stack);
	});
	fastifyInstance.register(filesController(gachouCore, tmpDir.resolve("tmp")));
	port = await getPort();
	await fastifyInstance.listen(port);
});

afterEach(async () => {
	await fastifyInstance.close();
});

test("return a file for download if 'download' is set to tru", async () => {
	gachouCore.getOriginalMedium.mockReturnValue(
		Promise.resolve({
			file: async () => fixtureImages.path.still.png,
			mimeType: "image/jpg",
			stream: async () => fs.createReadStream(fixtureImages.path.still.png),
		})
	);
	const response = await got(`http://localhost:${port}/files/123.jpg?download=true`);
	expect(response.headers["content-disposition"]).toEqual(`attachment; filename="123.jpg"`);
});

test("return a file for view if 'download' is NOT set to true", async () => {
	gachouCore.getOriginalMedium.mockReturnValue(
		Promise.resolve({
			file: async () => fixtureImages.path.still.png,
			mimeType: "image/jpg",
			stream: async () => fs.createReadStream(fixtureImages.path.still.png),
		})
	);
	const response = await got(`http://localhost:${port}/files/123.jpg`);
	expect(response.headers["content-disposition"]).toEqual("inline");
});
