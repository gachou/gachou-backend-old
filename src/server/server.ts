import http from "http";
import { ICoreApi } from "../api";
import { setupGachou } from "../gacho-core/gachou-simple-setup";
import { createLogger } from "../utils/logger";
import createFastify from "fastify";
import fastifyCors from "fastify-cors";
import { filesController } from "./controllers/files";
import multer from "fastify-multer";
import { statusController } from "./controllers/status";
import { metadataController } from "./controllers/metadata";
import { AddressInfo } from "net";
import swagger from "fastify-swagger";
import swaggerValidation from "openapi-validator-middleware";
import path from "path";

const logger = createLogger("gachou-backend:server");

interface Options {
	port: number;
	mediaDir: string;
	cacheDir: string;
	mongoDbUrl: string;
	mongoDbUsername?: string;
	mongoDbPassword?: string;
	staticDir?: string;
	host?: string;
}

interface GachouServer {
	close(): Promise<void>;
	address: AddressInfo | string | null;
}

const OPEN_API_FILE = "./src/server/openapi/gachou.openapi.yaml";

export async function server({
	mediaDir,
	cacheDir,
	port,
	host,
	mongoDbUrl,
	mongoDbUsername,
	mongoDbPassword,
}: Options): Promise<GachouServer> {
	const fastify = createFastify<http.Server>({ logger });

	swaggerValidation.init(OPEN_API_FILE, {
		framework: "fastify",
	});
	fastify.register(
		swaggerValidation.validate({
			skiplist: ["^/documentation", "^/files"],
		})
	);

	fastify.register(fastifyCors, { origin: ["http://localhost:3000"] });

	fastify.setErrorHandler((error, request, reply) => {
		if (error instanceof swaggerValidation.InputValidationError) {
			return reply.status(400).send(JSON.stringify(error));
		}
		logger.error({ msg: "Error in request", method: request.method, url: request.url });
		reply.status(error.statusCode || 500).send(JSON.stringify(error));
	});

	fastify.addHook("onClose", () => gachouCore.shutdown());

	const gachouCore: ICoreApi = await setupGachou({
		mediaDir,
		cacheDir,
		mongoDbUrl,
		mongoDbUsername,
		mongoDbPassword,
	});
	fastify.register(multer.contentParser);
	fastify.register(swagger, {
		routePrefix: "/documentation",
		mode: "static",
		specification: {
			path: OPEN_API_FILE,
			baseDir: path.resolve(path.dirname(OPEN_API_FILE)),
		},
		exposeRoute: true,
	});

	fastify.register(filesController(gachouCore, cacheDir));
	fastify.register(statusController(gachouCore));
	fastify.register(metadataController(gachouCore));

	await fastify.listen({ port, host });
	await fastify.swagger();
	return {
		async close() {
			await fastify.close();
		},
		address: fastify.server.address(),
	};
}
