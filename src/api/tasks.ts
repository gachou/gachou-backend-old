import {
	Filename,
	IMediaFile,
	IMediaResponse,
	IMetadata,
	ITaskStatus,
	IWritableMetadata,
	ScaleSpec,
} from "./model";

/**
 * API for long running functions to be invoked by plugins and gachou-core.
 * This API is internal and should only be accessed by the gachou-core-implementation
 *
 */
export interface ITasksApi {
	/**
	 * Convert a file to a normalized for that can store metadata.
	 * No metadata is actually stored, but the metadata parameter can
	 * be used to determine the best
	 * @param {Filename} file
	 * @param {Date} modificationDate the modification date of the file can be used
	 *   to set the creation date if no other tags are available in the file
	 * @return {Promise<Filename>}
	 */
	normalize(file: Filename, modificationDate: Date): Promise<Filename>;

	/**
	 * Create and store a scaled version of an image and return it
	 * @param {string} name the id of the image in the media-store
	 * @param {ScaleSpec} spec the scale specification
	 * @return {Promise<IMediaResponse>} the scaled image
	 */
	scaleImage(name: string, spec: ScaleSpec): Promise<IMediaResponse>;

	extractMetadata(file: Filename): Promise<IMetadata>;

	updateMetadata(name: string, metadata: Partial<IWritableMetadata>): Promise<void>;

	/**
	 * Add a file to the media-store and the metadata-index and create thumbnails
	 * @param {Filename} file the path to the local file
	 * @param {Date} modificationDate
	 * @return {Promise<IMediaFile>}
	 */
	addFile(file: Filename, modificationDate: Date): Promise<IMediaFile>;

	status(): ITaskStatus;

	/**
	 * Start background tasks for creating all needed thumbnails of a picture, but
	 * ignore the result. Log errors to the debug log
	 * @param {string} name
	 */
	ensureAllThumbs(name: string): void;

	waitForIdle(): Promise<void>;
}
