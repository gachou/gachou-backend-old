import { Readable } from "stream";
import { DateTime } from "luxon";

export interface IMediaFile {
	/**
	 * A unique name for this file
	 */
	name: string;

	/**
	 * Metadata for this file
	 */
	metadata: IMetadata;
}

export interface IMetadata extends IWritableMetadata {
	mimeType: string;
	/**
	 * Size of the video / image
	 */
	extents: IExtents;
}

export interface IWritableMetadata {
	creationDate: DateTime;
	rating?: number;
	tags?: string[];
}

/**
 * A metadata-update in the metadata-index
 *
 * * undefined means: "Don't touch"
 * * null means: "Delete"
 */
export type IMetadataUpdate = Partial<IWritableMetadata>;

/**
 * Dimensions of images and videos
 */
export interface IExtents {
	/**
	 * Width in pixels (visible width)
	 */
	width: number;
	/**
	 * Height in pixels (visible height)
	 */
	height: number;
	/**
	 * Length (of a video) in milliseconds
	 */
	lengthMs?: number;
}

/**
 * A string representing a filename
 */
export type Filename = string;
/**
 * A string representing a scale specification (like "300x300")
 */
export type ScaleSpec = string;

/**
 * Configuration for the backend
 */
export interface IGachouConfig {
	requiredThumbnails: ScaleSpec[];
	tmpDir: string;
}

export interface IFullUpdateStatus {
	lastStarted?: Date;
	lastFinished?: Date;
	abortRequested?: boolean;
	messages: string[];
	fatalError?: string;
	totalFiles: number;
	stoppedBecause?: "done" | "aborted" | "error";
	done: number;
}

/**
 * Interface containing status-information about the whole system
 */
export interface IGachouStatus {
	tasks: ITaskStatus;
	fullUpdateStatus?: IFullUpdateStatus;
}

export interface ITaskStatus {
	running: number;
	waiting: number;
}

export interface IProgress {
	total: number;
	success: number;
	errors: number;
	done: number;
}

export interface IMediaResponse {
	/**
	 * The mime-type of the file
	 */
	mimeType: string;

	/**
	 * A functiom that stores a copy of the file locally and
	 * returns a promise for the resulting path.
	 */
	file: () => Promise<string>;

	/**
	 * Function that opens a read-stream for the contents of this file
	 */
	stream: (options?: IStreamOptions) => Promise<Readable>;
}

/**
 * Options for retrieving the stream of a file
 */
export interface IStreamOptions {
	/**
	 * The starting byte
	 */
	start: number;
	/**
	 * The last byte
	 */
	end: number;
}

export interface IMetadataEntry {
	/**
	 * A unique name for this file
	 */
	name: string;

	/**
	 * The metadata of the file
	 */
	metadata: IMetadata;
}

export interface IFacet {
	/**
	 * The id of the facet and the query parameter in the search request
	 */
	id: string;

	/**
	 * The display name of the facet
	 */
	label: string;

	/**
	 * The possible values of this facet
	 */
	values: IFacetValue[];
}

export interface IFacetValue {
	/**
	 * The value of the query string parameter
	 */
	value: string;

	/**
	 * True, if this facet value is currently applied to the query
	 */
	active: boolean;

	/**
	 * Number of search results if this facet value is used in a query
	 */
	count: number;
}

export interface IMetadataQuery {
	// The year as left-padded string (4 digits)
	year?: string;
	// The month as left-padded string (2 digits)
	month?: string;
	// The mime-type
	mimeType?: string;
}

export interface ISearchContext {
	/**
	 * Index of the first element that was requested
	 */
	start: number;

	/**
	 * Number of elements requested
	 */
	limit: number;

	/**
	 * Total number of elements found by the search
	 */
	total: number;
}

export interface ISearchResult {
	facets: IFacet[];
	context: ISearchContext;
	results: IMetadataEntry[];
}
