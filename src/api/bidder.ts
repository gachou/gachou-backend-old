/**
 * Interface for classes that perform actions on certain file types, but
 * not on others. A Bidder is usually able to handle one file type,
 * and a DelegatingBidder is used to delegate the execution to the correct
 * implementation.
 */
export interface IBidder {
	/**
	 * Returns a number suggesting how well this class can process a given file
	 *
	 * Higher numbers mean "more suited". A negative number means that the class
	 * cannot handle this file type.
	 *
	 *
	 * @param {string} file the path to the file
	 * @param {string} mimeType the mimeType of the file
	 * @retrn {Promise<number>}
	 */
	bid(file: string, mimeType: string): Promise<number>;
}
