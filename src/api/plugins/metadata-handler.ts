import { Filename, IMetadata, IMetadataUpdate } from "../model";

export interface IMetadataHandler {
	/**
	 * Extract metadata from a given file
	 * @param {string} file the path to the file
	 * @param {Date} modificationDate the last modification date of the file (this date differ from the date returned
	 *   by `fs.stat(...)`. If an uploaded file was temporarily stored, the original modification date may be passed along with the
	 *   upload and
	 * @return {Promise<IMetadata>}
	 */
	extractMetadata(file: Filename, modificationDate?: Date): Promise<IMetadata>;

	/**
	 * Updates the metadata in the given file.
	 * This operation must support concurrent operations. It may be called
	 * multiple times on the same object at the same time.
	 *
	 * @param {string} file
	 * @param {IMetadata} metadata
	 * @return {Promise<void>}
	 */
	updateMetadata(file: string, metadata: IMetadataUpdate): Promise<void>;
}
