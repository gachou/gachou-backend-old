import { Filename, IMediaResponse } from "../model";

export interface IStoreApi {
	/**
	 * Get a specific file by its path
	 * @param {string} name an id, name or path of this file inside the store
	 *  this may be a local filename
	 * @return {Promise<IMediaResponse>}
	 */
	get(name: string): Promise<IMediaResponse>;

	/**
	 * Add a file to the store.
	 * @param {string} fullPath the path to the new file
	 * @param {string} creationDate the date of birth of this file
	 * @return {Promise<string>} the unique name for retrieving this file
	 */
	add(fullPath: string, creationDate: Date): Promise<string>;

	/**
	 * Move a file to the trash directory (for deletion)
	 * @param {string} name the name of the file
	 */
	moveToTrash(name: string): Promise<void>;

	restoreFromTrash(name: string): Promise<void>;
	/**
	 * Copy a given file to a tmp dir, call the callback that modifies it
	 * and then copy it back
	 * @param {string} name
	 * @param callback the callback-function that is called when tmp-dir is ready
	 * @return {Promise<void>}
	 */
	update(name: string, callback: (fullPath: Filename) => Promise<void>): Promise<void>;

	/**
	 * Iterate all files. The function returns all files found in the store.
	 * The resulting files can be user as name for "get".
	 */
	iterate(): AsyncGenerator<string>;

	/**
	 * Returns the total number of files in the store
	 */
	count(): Promise<number>;

	/**
	 * Update a thumbnail
	 * @param {string} name
	 * @param {string} spec
	 * @param {string} fullPath
	 */
	putThumb(name: string, spec: string, fullPath: string): Promise<void>;

	/**
	 * Get the thumbnails of a specific file by its name
	 * @param {string} name an id, name or path of this file inside the store
	 *  this may be a local filename
	 * @param {string} spec the scaler spec
	 * @return {Promise<IMediaResponse>}
	 */
	getThumb(name: string, spec: string): Promise<IMediaResponse>;

	removeThumbnails(name: string): Promise<void>;
}
