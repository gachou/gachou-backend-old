import { IBidder } from "../bidder";
import { Filename } from "../model";

/**
 * Interface to be implemented by thumbnailer plugins
 */
export interface IThumbnailer extends IBidder {
	/**
	 * Create a thumbnail for the given file.
	 *
	 * The operation does not need to be atomic. Atomicity is ensured by the surrounding framework.
	 * The parameter "thumbFilePrefix" is the path to a file (without extension) that can be used as target file.
	 * Implementations of this interface can use any paths that start with this value, either as intermediate files
	 * or as target file. The target file (i.e. the file containing the final thumbnail must be returned as promise.
	 * All files starting with this parameter are removed after the execution of the method, so if you stick to the convention,
	 * you don't have to clean up yourself.
	 *
	 *
	 * @param originalFile the original file
	 * @param spec the size and scale specification
	 * @param thumbFilePrefix the path-prefix of a file that can be used as target file and for intermediate files
	 * @return a promise for the name of the file that is used
	 */
	createThumbnail(
		originalFile: Filename,
		spec: IParsedScaleSpec,
		thumbFilePrefix: string
	): Promise<Filename>;
}

/**
 * A parsed specification of thumbnails
 */
export interface IParsedScaleSpec {
	/**
	 * Target height of the thumbnail
	 */
	height: number;
	/**
	 * Target width of the thumbnail
	 */
	width: number;
	/**
	 * True, if the thumbnail should be cropped to match the extact extents
	 * False, if the thumbnail should be fit within the box specified by width and height,
	 * without changing the aspect ratio
	 */
	crop: boolean;
}
