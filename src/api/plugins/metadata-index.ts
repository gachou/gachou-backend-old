/**
 * The api of the metadata-index
 */

import { IMetadataEntry, IMetadataQuery, IMetadataUpdate, ISearchResult } from "../model";

export { IFacet, IFacetValue, IMetadataEntry, IMetadataQuery, ISearchResult } from "../model";

export interface IMetadataIndex {
	/**
	 * Add a new file to the metadata-db
	 * @param entry
	 */
	add(entry: IMetadataEntry): Promise<void>;

	/**
	 * Update a file in the metadata-db
	 * WARNING: Not yet implemented!
	 */
	update(name: string, metadata: IMetadataUpdate): Promise<void>;

	remove(name: string): Promise<void>;

	/**
	 * This notifies the metadata-index that a synchronization with the media-store
	 * is starting. The function returns a number that is then passed to the `syncFinished`-function
	 * in order to delete surplus entries in the database.
	 *
	 * @return {number}
	 */
	syncStarted(): Promise<number>;

	/**
	 * This notifies that media-store that a synchronization is finished. Documents that have not been
	 * added to the database since syncStarted-operation with this number, should be deleted
	 * @param {number} syncId the return value of the 'syncStarted`-function
	 */
	syncFinished(syncId: number): Promise<void>;

	find(query: IMetadataQuery, start, limit): Promise<ISearchResult>;

	/**
	 * Read and return metadata for a single
	 * @param {string} name
	 * @return {Promise<IMetadataEntry>}
	 */
	read(name: string): Promise<IMetadataEntry | null>;

	shutdown(): Promise<void>;
}
