import { IBidder } from "../bidder";

export interface IMediaNormalizer extends IBidder {
	/**
	 * Creates a copy of the file, converting it to a normalized
	 * format for the given media-type, if necessary. The format must be
	 * able to store XMP-Tags as well as a creation date.
	 * Implementations must make sure that existing metadata is retained
	 * @param {string} file the path to the file
	 * @param {string} mimeType the mime-type of the file (derived by the normalizer-facade)
	 * @param modificationDate the last modification date of the uploaded file
	 * @param tmpDir
	 * @return {Promise<string>} path to the converted
	 */
	normalize(
		file: string,
		mimeType: string,
		modificationDate: Date,
		tmpDir: string
	): Promise<string>;

	/**
	 * Returns a number suggesting how well the normalizer can normalize the given file.
	 *
	 * Higher numbers mean "more suited". A negative number means that the normalizer
	 * cannot handle this file type.
	 *
	 *
	 * @param {string} file the path to the file
	 * @param {string} mimeType the mimeType of the file
	 * @retrn {Promise<number>}
	 */
	bid(file: string, mimeType: string): Promise<number>;
}

export class NoNormalizerFoundError extends Error {
	public readonly mimeType: string;

	constructor(message: string, mimeType: string) {
		super(message);
		this.mimeType = mimeType;
	}
}
