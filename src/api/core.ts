import {
	IGachouStatus,
	IMediaFile,
	IMediaResponse,
	IMetadataEntry,
	IMetadataQuery,
	ISearchResult,
	ScaleSpec,
} from "./model";

/**
 * This is the api that is used by the rest-interface.
 * Its implementation ties the different components together
 */
export interface ICoreApi {
	/**
	 * Add a media file to the storage (includes metadata extraction,
	 * normalization and indexing).
	 *
	 * The file modification date is used as creation date if the image does
	 * not have any XMP, Exif or Quicktime-Tags that show this date.
	 *
	 * @param {string} name
	 * @param {Date} modificationDate the file modification date
	 */
	addFile(name: string, modificationDate: Date): Promise<IMediaFile>;

	/**
	 * Remove a file (not completely, but move it to the trash directory
	 */
	moveFileToTrash(name: string): Promise<void>;

	/**
	 * Restore a file (from the trash directory)
	 */
	restoreFileFromTrash(name: string): Promise<void>;

	/**
	 * Iterate the media store and update the index metadata-index
	 * to make sure it is up-to-date
	 */
	updateMetadataIndex(): Promise<void>;

	status(): IGachouStatus;

	getOriginalMedium(name: string): Promise<IMediaResponse>;

	getThumbnail(name: string, spec: ScaleSpec): Promise<IMediaResponse>;

	find(query: IMetadataQuery, start: number, limit: number): Promise<ISearchResult>;

	/**
	 * Retrieve the metadata for a single file
	 * @param {string} name
	 * @return {Promise<IMetadataEntry>}
	 */
	getMetadata(name: string): Promise<IMetadataEntry>;

	/**
	 * Clear intervals that have be registered
	 */
	shutdown(): Promise<void>;
}
